import { Component } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { ActivatedRoute } from "@angular/router";
import { Category } from "../../../core/models/category.model";
import { CategoryApi } from "../../../core/api/category.api";
import { CollectionResponseInterface } from "../../../core/interfaces/system/collection-response.interface";

const PARAM_ID = 'id';

@Component({
	selector: 'categories',
	template: `<category-grid [categories]="categories"></category-grid>`
})
export class CategoriesComponent {

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * Categories
	 */
	categories: Category[];

	constructor(
		private categoryApi: CategoryApi,
		private route: ActivatedRoute
	) {
		this.subscriptions$.push(
			this.route.params
				.subscribe((params) => {
					let parentId = params[PARAM_ID]
						? params[PARAM_ID]
						: 0;

					let findAllParams = {};
					findAllParams[CategoryApi.PARAM_PARENT_ID] = parentId;

					this.categoryApi.findAll(findAllParams)
						.subscribe(
							(response: CollectionResponseInterface<Category>) => {
								this.categories = response.data
							}
						)
				})
		)
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => subscription.unsubscribe()
		)
	}
}

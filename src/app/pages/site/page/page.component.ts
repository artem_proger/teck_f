import { Component } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { ActivatedRoute, Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { SettingsApi } from "../../../core/api/settings.api";
import { PageService } from "../../../core/services/page.service";
import { AppConfig } from "../../../app.config";
import { SystemTitle } from "../../../core/services/system/system-title.service";

const PARAM_PAGE = 'page';

@Component({
	selector: 'page',
	templateUrl: './page.component.html',
    styleUrls: ['./page.component.css']
})
export class PageComponent {

	/**
	 * Page title
	 */
	title: string;

	/**
	 * Current page setting key
	 */
	pageKey: any;

	/**
	 * Content of current page
	 */
	content: any;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(
		private settingsApi: SettingsApi,
		private pageService: PageService,
	    private route: ActivatedRoute,
	    private router: Router,
		private sanitizer: DomSanitizer,
        private systemTitle: SystemTitle
	) { }

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {
		this.subscriptions$.push(
			/**
			 * Subscribe for current page key param
			 */
			this.route.params
				.subscribe(
					(params) => {
						this.pageKey = this.pageService
							.getSettingKeyByParam(params[PARAM_PAGE]);
						this.title = this.pageService
							.getTitleByParam(params[PARAM_PAGE]);

						this.systemTitle.setTitle(this.title);

						if (!this.pageKey) {
							this.router.navigate([AppConfig.ROUTE_404]);
						}

						let findAllParams = {};
						findAllParams[SettingsApi.PARAM_IN] = [
							this.pageKey
						].join(',');

						/**
						 * Get current page setting content
						 */
						this.settingsApi.findAll(findAllParams)
							.subscribe((response: any) => {
								this.content = this.sanitizer
									.bypassSecurityTrustHtml(
										response[this.pageKey]
									);
							});
					}
				)
		);
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => subscription.unsubscribe()
		)
	}
}

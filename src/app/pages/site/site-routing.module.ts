import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SiteComponent } from "./site.component";
import { HomeComponent } from "./home/home.component";
import { CategoriesComponent } from "./categories/categories.component";
import { ProductsComponent } from "./products/products.component";
import { ProductDetailsComponent } from "./products/details/details.component";
import { PageComponent } from "./page/page.component";
import { PRODUCT_LIST_TITLE } from "../../core/constants/products.constant";

const siteRoutes: Routes = [
	{
		path: '',
		component: SiteComponent,
		data: {title: 'Site component'},
		children: [
			{
				path: '',
				component: HomeComponent,
				data: {title: 'Главная'}
			},
			{
				path: 'categories',
				component: CategoriesComponent,
				data: {title: 'Категории'}
			},
			{
				path: 'categories/:id',
				component: CategoriesComponent,
				data: {title: 'Категории'}
			},
			{
				path: 'products',
				component: ProductsComponent,
				data: {title: PRODUCT_LIST_TITLE}
			},
			{
				path: 'products/:id',
				component: ProductDetailsComponent,
				data: {title: 'Детали продукта...'}
			},
			{
				path: 'page/:page',
				component: PageComponent,
				data: {title: false}
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(siteRoutes)],
	exports: [RouterModule]
})
export class SiteRoutingModule { }

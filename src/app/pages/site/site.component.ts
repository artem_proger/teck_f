import { Component } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { SearchToggleService } from "../../core/services/search-toggle.service";
import { SettingsApi } from "../../core/api/settings.api";
import { SettingsService } from "../../core/services/settings.service";

const AOS = require('aos');

declare var $: any;

@Component({
	selector: 'site',
	templateUrl: './site.component.html',
	styleUrls: ['./site.component.css']
})
export class SiteComponent {
	
	/**
	 * Search status
	 *
	 * @type {boolean}
	 */
	isSearchActive: boolean = false;

	/**
	 * Settings
	 */
	settings: any;

	/**
	 * Settings keys
	 */
	settingKeys: any;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(
            private searchToggleService: SearchToggleService,
	    private settingsApi: SettingsApi,
	    private settingsService: SettingsService
	) {
		this.settingKeys = this.settingsService.getNameMapping();
	}

	ngOnInit() {
        AOS.init({
            disable: 'mobile'
        });

        /**
         * Subscribe on search status to rotate site content
         */
        this.subscriptions$.push(
                this.searchToggleService.getSearchStatusObservable()
                        .subscribe((isActive: boolean) => {
                                this.isSearchActive = isActive;
                        })
        );

        var settingsFindAllParams = {};
        settingsFindAllParams[SettingsApi.PARAM_IN] = this.settingsService
                .getGeneralSettings()
                .join(',');

        /**
         * Get settings
         */
        this.settingsApi.findAll(settingsFindAllParams)
                .subscribe(
                        (response: any) => {
                                this.settings = response;
                        }
                );
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => {
				subscription.unsubscribe();
			}
		)
	}
}

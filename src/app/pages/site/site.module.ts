import { NgModule } from "@angular/core";
import { SiteRoutingModule } from "./site-routing.module";
import { SiteComponent } from "./site.component";
import { HomeComponent } from "./home/home.component";
import { CommonModule } from "@angular/common";
import { CategoriesComponent } from "./categories/categories.component";
import { ProductsComponent } from "./products/products.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ProductsFilterComponent } from "./products/filter/filter.component";
import { ProductDetailsComponent } from "./products/details/details.component";
import { NgxGalleryModule } from "ngx-gallery";
import { SliderComponent } from "./home/slider/slider.component";
import { FormsModule } from "@angular/forms";
import { ImageGalleryComponent } from "./products/details/image-gallery/image-gallery.component";
import { PageComponent } from "./page/page.component";
import { SearchComponent } from "../../shared/components/site/search/search.component";
import { ProductFetcher } from "../../core/services/product-fetcher.service";
import { PageService } from "../../core/services/page.service";
import { SettingsService } from "../../core/services/settings.service";
import { SettingsApi } from "../../core/api/settings.api";
import { SearchToggleService } from "../../core/services/search-toggle.service";
import { SliderService } from "../../core/services/slider.service";
import { SliderApi } from "../../core/api/slider.api";
import { ProductService } from "../../core/services/product.service";
import { ProductApi } from "../../core/api/product.api";
import { CategoryService } from "../../core/services/category.service";
import { FooterComponent } from "../../shared/components/site/footer/footer.component";
import { CategoryApi } from "../../core/api/category.api";
import { HeaderComponent } from "../../shared/components/site/header/header.component";
import { CategoryGridComponent } from "../../shared/components/site/category-grid/category-grid.component";
import { SharedModule } from "../../shared/shared.module";
import { QueryParamService } from "../../core/services/system/query-param.service";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SiteRoutingModule,
		SharedModule,
		NgbModule,
		NgxGalleryModule,
	],
	declarations: [
		SiteComponent,
		HomeComponent,
		CategoriesComponent,
		ProductsComponent,
		ProductsFilterComponent,
		ProductDetailsComponent,
		SliderComponent,
		HeaderComponent,
		FooterComponent,
		SearchComponent,
		ImageGalleryComponent,
		PageComponent,
		CategoryGridComponent
	]
})
export class SiteModule { }
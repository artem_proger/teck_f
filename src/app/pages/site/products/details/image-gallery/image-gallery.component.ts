import { Component, Input, ElementRef } from "@angular/core";
import { ImagePackage } from "../../../../../core/models/image-package.model";
import { ProductService } from "../../../../../core/services/product.service";

declare var $: any;

import * as _ from "lodash";

const THUMBNAIL_CHUNK = 5;
const SELECTOR_MAIN_IMAGE_CONTAINER = '.main-image';
const EVENT_ZOOM_DESTROY = 'zoom.destroy';
const ZOOM_OPTIONS = (url: string) => {
	return {
		url: url,
		on: 'grab',
		magnify: 1.5,
		duration: 240
	}
};

@Component({
	selector: 'image-gallery',
	templateUrl: './image-gallery.component.html',
	styleUrls: ['./image-gallery.component.css']
})
export class ImageGalleryComponent {

	/**
	 * Selected image
	 */
	@Input()
	selectedImage: any;

	/**
	 * ImagePackages
	 */
	@Input()
	imagePackages: ImagePackage[];

	/**
	 * Selected ImagePackage
	 */
	selectedImagePackage: ImagePackage;

	/**
	 * jQuery image container object
	 */
	mainImage: any;
	
	constructor(
		private el: ElementRef,
		private productService: ProductService
	) { }

	ngOnInit() {
		this.selectedImagePackage = this.getSelectedImagePackage();
		this.mainImage = $(this.el.nativeElement)
			.find(SELECTOR_MAIN_IMAGE_CONTAINER);

		this.initZoom(this.getImageUrl(this.selectedImagePackage));
	}

	/**
	 * Get selected ImagePackage
	 *
	 * @returns {ImagePackage}
	 */
	getSelectedImagePackage() {
		return this.imagePackages.find((imagePackage: ImagePackage) => {
			return _.isEqual(imagePackage.filenames, this.selectedImage);
		});
	}

	/**
	 * Get main ImagePackage's image URL
	 *
	 * @param imagePackage
	 *
	 * @returns {string}
	 */
	getImageUrl(imagePackage: ImagePackage) {
		return this.productService.getImagePackageUrl(
			imagePackage, ProductService.SIZE_MAX
		);
	}

	/**
	 * Get ImagePackage's thumbnail image URL
	 *
	 * @param imagePackage
	 *
	 * @returns {string}
	 */
	getThumbnailUrl(imagePackage: ImagePackage) {
		return this.productService.getImagePackageUrl(
			imagePackage, ProductService.SIZE_MINI
		);
	}

	/**
	 * Get range of ImagePackages
	 *
	 * @returns {ImagePackage[]}
	 */
	getActualImagePackages() {
		return this.imagePackages.slice(0, THUMBNAIL_CHUNK);
	}

	/**
	 * Move image carousel to the left
	 */
	toTheLeft() {
		var moveImagePackage = this.imagePackages[this.imagePackages.length - 1];
		this.imagePackages.splice(this.imagePackages.length - 1, 1);
		this.imagePackages = [moveImagePackage].concat(this.imagePackages);

	}

	/**
	 * Move image carousel to the right
	 */
	toTheRight() {
		var moveImagePackage = this.imagePackages[0];
		this.imagePackages.splice(0, 1);
		this.imagePackages.push(moveImagePackage);
	}

	/**
	 * Select ImagePackage
	 *
	 * @param imagePackage
	 */
	selectImagePackage(imagePackage: ImagePackage) {
		this.selectedImagePackage = imagePackage;
		this.initZoom(this.getImageUrl(this.selectedImagePackage));
	}

	/**
	 * Init zoom on main image
	 * 
	 * @param imageUrl
	 */
	initZoom(imageUrl: string) {
		this.mainImage.trigger(EVENT_ZOOM_DESTROY);
		this.mainImage.zoom(ZOOM_OPTIONS(imageUrl));
	}
}

import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import { Product } from "../../../../core/models/product.model";
import { ProductApi } from "../../../../core/api/product.api";
import { SystemTitle } from "../../../../core/services/system/system-title.service";

const PARAM_ID = 'id';

@Component({
	selector: 'product-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.css']
})
export class ProductDetailsComponent {

	/**
	 * Product
	 */
	product: Product;

	/**
	 * Component subscriptions
	 * 
	 * @type {Array}
	 */
	subscription$: Subscription[] = [];

	constructor(
		private route: ActivatedRoute,
	    private productApi: ProductApi,
		private systemTitle: SystemTitle
	) {
		this.subscription$.push(
			this.route.params
				.subscribe((params) => {
					this.productApi.find(params[PARAM_ID])
						.subscribe(
							(product: Product) => {
								this.product = product;
								this.systemTitle.setTitle(this.product.title);
							}
						)
				})
		)
	}
}

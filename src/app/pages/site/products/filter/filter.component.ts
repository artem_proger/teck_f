import { Component, ElementRef, Input, EventEmitter, Output } from "@angular/core";
import { fromEvent } from "rxjs/observable/fromEvent";
import { Subscription } from "rxjs/Rx";
import { interval } from "rxjs/observable/interval";
import { AppConfig } from "../../../../app.config";

declare var $: any;

const DEFAULT_FILTER = {
	price_from: 300,
	price_to: 600
};

const SELECTOR_FILTER = '#price_filter';
const SELECTOR_APPLY_BTN = '#apply-filter-btn';
const SELECTOR_CLEAR_BTN = '#clear-filter-btn';
const EVENT_CLICK = 'click';
const ION_RANGE_SLIDER_OPTIONS = (filter: any) => {
	return {
		type: "double",
		min: 0,
		max: 1000,
		from: filter.price_from,
		to: filter.price_to,
		step: 20,
		grid: true,
		grid_snap: true,
		onChange: (data: any) => {
			filter.price_from = data.from;
			filter.price_to = data.to;
		}
	}
};

@Component({
	selector: 'products-filter',
	templateUrl: './filter.component.html',
	styles: ['.price-range { margin-top: 0 }']
})
export class ProductsFilterComponent {

	/**
	 * Filter
	 */
	@Input()
	filter: any;

	/**
	 * Emit when apply some filter params
	 *
	 * @type {EventEmitter<any>}
	 */
	@Output()
	filterApply: EventEmitter<any> = new EventEmitter<any>();

	/**
	 * Submit form source
	 */
	submitFormSource: any;

	/**
	 * Clear filter source
	 */
	clearFilterSource: any;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(private el: ElementRef) {
		this.filter = DEFAULT_FILTER;
	}

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {

		/**
		 * Init sources
		 */
		this.submitFormSource = fromEvent(
			$(this.el.nativeElement)
				.find(SELECTOR_APPLY_BTN)[0],
			EVENT_CLICK
		);
		this.clearFilterSource = fromEvent(
			$(this.el.nativeElement)
				.find(SELECTOR_CLEAR_BTN)[0],
			EVENT_CLICK
		);
		
		this.subscriptions$.push(
			/**
			 * Subscribe on clear filter
			 */
			this.clearFilterSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					this.filterApply.next({});
				}),
			/**
			 * Subscribe on form submit
			 */
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					this.filterApply.next(this.filter);
				})
		);

		/**
		 * Init price range filter
		 */
		var priceFilter = $(this.el.nativeElement).find(SELECTOR_FILTER);
		priceFilter.ionRangeSlider(ION_RANGE_SLIDER_OPTIONS(this.filter));

	}

	ngOnDestroy() {
		this.subscriptions$.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}
}
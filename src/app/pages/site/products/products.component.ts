import { Component } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { Product } from "../../../core/models/product.model";
import { Meta } from "../../../core/interfaces/system/meta.interface";
import { QueryParamService } from "../../../core/services/system/query-param.service";
import { ProductFetcher } from "../../../core/services/product-fetcher.service";
import { ProductService } from "../../../core/services/product.service";
import { ProductApi } from "../../../core/api/product.api";
import { PaginatedResponseInterface } from "../../../core/interfaces/system/paginated-response.interface";

@Component({
	selector: 'products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.css']
})
export class ProductsComponent {

	/**
	 * Products
	 */
	products: Product[];

	/**
	 * Meta information
	 */
	meta: Meta;

	/**
	 * Query params for fetching Products
	 */
	params: any;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(
		private productApi: ProductApi,
		private productService: ProductService,
		private productFetcher: ProductFetcher,
		private queryParam: QueryParamService
	) {
		let currentParams = this.queryParam.getCurrentParams();
		this.productFetcher.applyParams(currentParams);

		this.subscriptions$.push(
			/**
			 * Subscripe for fetching products
			 */
			this.productFetcher.getObservable()
				.subscribe((response: PaginatedResponseInterface<Product>) => {
					this.meta = response.meta;
					this.products = response.data;
				})
		);
	}

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {
		this.productFetcher.fetch();
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => {
				subscription.unsubscribe();
			}
		);

		this.productFetcher.clearFilter();
		this.queryParam.clearQueryString();
	}

	/**
	 * Fetch products by params
	 * 
	 * @param params
	 */
	fetchProducts(params: any = null) {
		this.productApi.findAll(params)
			.subscribe((response: PaginatedResponseInterface<Product>) => {
				this.meta = response.meta;
				this.products = response.data;
			})
	}

	/**
	 * Apply params to filter
	 * 
	 * @param newParams
	 * @param page
	 */
	applyParams(newParams: any = null, page: number = null) {
		this.params = Object.assign(
			newParams ? newParams : this.params,
			{
				page: (page) ? page : this.meta.page
			}
		);
		var queryParams = Object.assign({}, this.params);
		delete queryParams[ProductFetcher.PARAM_CATEGORY];
		this.queryParam.applyParams(queryParams);
	}

	/**
	 * Change Products page
	 *
	 * @param page
	 */
	pageChange(page: number) {
		var params = { page: page };
		this.productFetcher.applyParams(params);
		this.queryParam.applyParams(params, true);
		this.productFetcher.fetch();
		window.scrollTo(0, 0);
	}

	/**
	 * Get Product details route
	 *
	 * @param id
	 *
	 * @returns {string}
	 */
	getProductDetailsRoute(id: number) {
		return ProductService.ROUTE_PRODUCT_DETAILS(id);
	}

	/**
	 * Get Product image URL
	 *
	 * @param product
	 *
	 * @returns {undefined|string}
	 */
	getProductImage(product: Product) {
		return this.productService.getImageUrl(product, ProductService.SIZE_MAX);
	}

	/**
	 * On filter change
	 * @param filter
	 */
	onFilterChange(filter: any) {
		var filterParams = {};

		if (Object.keys(filter).length === 0) {
			this.productFetcher.clearFilter();
			this.queryParam.applyParams(filterParams);
		} else {
			filterParams[ProductFetcher.PARAM_PRICE_FROM] = filter.price_from;
			filterParams[ProductFetcher.PARAM_PRICE_TO] = filter.price_to;
			this.queryParam.applyParams(filterParams, true);
		}
		this.productFetcher.applyParams(filterParams);
		this.productFetcher.fetch();
	}
}

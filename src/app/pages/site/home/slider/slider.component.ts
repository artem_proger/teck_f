import { Component, Input } from "@angular/core";
import { Slide } from "../../../../core/models/slide.model";
import { SliderService } from "../../../../core/services/slider.service";

@Component({
	selector: 'home-slider',
	templateUrl: './slider.component.html',
	styleUrls: ['./slider.component.css']
})
export class SliderComponent {

	/**
	 * Slides
	 */
	@Input() slides: Slide[];

	constructor(private sliderService: SliderService) { }

	/**
	 * Get Slide image URL
	 *
	 * @param slide
	 *
	 * @returns {null|string}
	 */
	getSlideImage(slide: Slide) {
		return this.sliderService.getImageUrl(slide, SliderService.SIZE_MAX);
	}

	getPositionMapping() {
		return this.sliderService.getPositionMapping();
	}
}
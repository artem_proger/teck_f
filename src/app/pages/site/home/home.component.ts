import { Component } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { Category } from "../../../core/models/category.model";
import { Slide } from "../../../core/models/slide.model";
import { CategoryApi } from "../../../core/api/category.api";
import { SliderApi } from "../../../core/api/slider.api";
import { CollectionResponseInterface } from "../../../core/interfaces/system/collection-response.interface";

@Component({
	selector: 'home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent {

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * Categories
	 */
	categories: Category[];

	/**
	 * Slides
	 */
	slides: Slide[];

	constructor(
		private categoryApi: CategoryApi,
	    private sliderApi: SliderApi
	) { }

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {
		let findAllParams = {};
		findAllParams[CategoryApi.PARAM_PARENT_ID] = 0;

		this.categoryApi.findAll(findAllParams)
			.subscribe((response: CollectionResponseInterface<Category>) => {
				this.categories = response.data;
			});

		this.sliderApi.findAll()
			.subscribe((response: CollectionResponseInterface<Slide>) => {
				this.slides = response.data;
			});
	}

	ngOnDestroy() {
		this.subscriptions$.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}
}
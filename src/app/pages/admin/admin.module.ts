import { NgModule } from '@angular/core';
import { AdminRoutingModule } from "./admin-routing.module";
import { LoginComponent } from "./login/login.component";
import { FormsModule } from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from "../../shared/shared.module";


@NgModule({
	imports: [
		AdminRoutingModule,
		SharedModule,
		FormsModule,
		NgbModule
	],
	declarations: [
		LoginComponent
	]
})
export class AdminModule { }
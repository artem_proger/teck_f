import { NgModule } from "@angular/core";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";
import { ContextMenuModule } from "ngx-contextmenu/lib/ngx-contextmenu";
import { CommonModule } from "@angular/common";
import { SimpleNotificationsModule } from "angular2-notifications";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { SharedModule } from "../../../shared/shared.module";
import { SystemMessageService } from "../../../core/services/system/system-message.service";
import { ErrorMessageInterceptor } from "../../../core/interceptors/error-message.interceptor";
import { AuthInterceptor } from "../../../core/interceptors/auth-token.interceptor";

@NgModule({
	imports: [
		CommonModule,
		DashboardRoutingModule,
		ContextMenuModule,
		SharedModule
	],
	declarations: [
		DashboardComponent
	]
})
export class DashboardModule { }
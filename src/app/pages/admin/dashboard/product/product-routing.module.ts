import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProductComponent } from "./product.component";
import { EditComponent } from "./edit/edit.component";
import { ProductListComponent } from "./list/list.component";

const productRoutes: Routes = [
	{
		path: '',
		component: ProductComponent,
		data: { title: 'Продукты' },
		children: [
			{
				path: '',
				pathMatch: 'full',
				redirectTo: 'list'
			},
			{
				path: 'add',
				component: EditComponent,
				data: { title: 'Добавить продукт' }
			},
			{
				path: 'edit/:id',
				component: EditComponent,
				data: { title: 'Редактировать продукт' }
			},
			{
				path: 'list',
				component: ProductListComponent,
				data: { title: 'Список продуктов' }
			}
		]
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(productRoutes)
	],
	exports: [RouterModule]
})
export class ProductRoutingModule { }

import { Component, ElementRef } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { Product } from "../../../../../../core/models/product.model";
import { GalleryUploadStorage } from "../../../../../../core/classes/gallery-upload-storage.class";
import { ImagePackage } from "../../../../../../core/models/image-package.model";
import { ProductService } from "../../../../../../core/services/product.service";
import { ProductApi } from "../../../../../../core/api/product.api";

import * as _ from "lodash";

declare var $: any;

const SELECTOR_FILE_INPUT = '#upload_input';
const REQUEST_PARAM_GALLERY = 'gallery';
const REQUEST_PARAM_SELECTED = 'selected';

@Component({
	selector: 'dsh-gallery-upload',
	templateUrl: './gallery-upload.component.html',
	styleUrls: ['./gallery-upload.component.css'],
	providers: [GalleryUploadStorage]
})
export class GalleryUploadComponent {

	subscriptions$: Subscription[] = [];
	product: Product = new Product();
	imagePackages: ImagePackage[] = [];
	isEdit: boolean = false;
	selectedImagePackage: ImagePackage;
	fileInput: any;

	constructor(
		private galleryUploadStorage: GalleryUploadStorage,
		private productService: ProductService,
	    private productApi: ProductApi,
		private el: ElementRef
	) { }

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {
		this.subscriptions$.push(
			this.productService.getCurrentProductObservable()
				.subscribe(
					(product: Product) => {
						this.product = product;
						if (product.id) {
							this.initImagePackages(this.product);
							this.isEdit = true;
							this.fileInput = $(this.el.nativeElement.querySelector(SELECTOR_FILE_INPUT))[0];
						}
					}
				)
		);
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => subscription.unsubscribe()
		);
	}

	/**
	 * Append "selected" property to all ImagePackages
	 * 
	 * @param product
	 */
	initImagePackages(product: Product) {
		let imagePackages = product.gallery.map(
			(imagePackage: ImagePackage) => {
				imagePackage.selected = this.isSelectedImagePackage(
					imagePackage, product
				);
				return imagePackage;
			}
		);

		this.galleryUploadStorage.setImagePackages(imagePackages);
		this.imagePackages = this.galleryUploadStorage.getImagePackages();
	}


	/**
	 * Get index of selected ImagePackage
	 * 
	 * @returns {number}
	 */
	getIndexOfSelectedImagePackage() {
		var index = this.product.gallery.indexOf(this.selectedImagePackage);

		if (index !== -1) {
			return index;
		} else {
			return 0;
		}
	}

	/**
	 * Open browse file dialog
	 */
	openDialog() {
		this.fileInput.click();
	}

	/**
	 * Remove all images
	 */
	removeAllImages() {
		this.productApi.removeGallery(this.product.id)
			.subscribe((product: Product) => {
				this.productService.setProduct(product);
			});
	}

	/**
	 * Check is ImagePackage is selected for this product
	 *
	 * @param imagePackage
	 * @param product
	 *
	 * @returns {any}
	 */
	isSelectedImagePackage(imagePackage: ImagePackage, product: Product) {
		return _.isEqual(imagePackage.filenames, product.selected_image);
	}

	/**
	 * When user choose files which he wants to download
	 *
	 * @param event
	 */
	onInputChange(event: any) {
		let input = event.target;

		if (input.files.length == 0) {
			return;
		}

		var galleryFiles = input.files;
		var selected = this.getIndexOfSelectedImagePackage().toString();

		var formData = new FormData();

		for(let i = 0; i < galleryFiles.length; i++){
			formData.append(
				`${REQUEST_PARAM_GALLERY}[]`, galleryFiles[i], galleryFiles[i].name
			);
		}
		formData.append(REQUEST_PARAM_SELECTED, selected);

		this.productApi.uploadGallery(this.product.id, formData)
			.subscribe((product: Product) => {
				this.productService.setProduct(product);
				input.value = null;
			});
	}
}
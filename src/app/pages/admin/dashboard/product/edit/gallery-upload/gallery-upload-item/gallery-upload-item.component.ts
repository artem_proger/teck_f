import { Component, Input, ViewChild } from "@angular/core";
import { ContextMenuComponent } from "ngx-contextmenu/lib/contextMenu.component";
import { Product } from "../../../../../../../core/models/product.model";
import { ProductService } from "../../../../../../../core/services/product.service";
import { ImagePackage } from "../../../../../../../core/models/image-package.model";
import { ContextMenuOption } from "../../../../../../../core/classes/context-menu-option.class";
import { ProductApi } from "../../../../../../../core/api/product.api";
import { GalleryUploadStorage } from "../../../../../../../core/classes/gallery-upload-storage.class";

const IMAGE_SIZE = ProductService.SIZE_MINI;
const OPTION_SELECT = 'Выбрать как основную';
const OPTION_REMOVE = 'Удалить';

@Component({
	selector: 'dsh-gallery-upload-item',
	templateUrl: './gallery-upload-item.component.html',
	styleUrls: ['./gallery-upload-item.component.css']
})
export class GalleryUploadItemComponent {

	/**
	 * Product (needs only for id)
	 */
	@Input()
	product: Product;

	/**
	 * Current ImagePackage
	 */
	@Input()
	imagePackage: ImagePackage;

	/**
	 * Context menu options
	 */
	options: ContextMenuOption[];

	constructor(
		private galleryUploadStorage: GalleryUploadStorage,
	    private productService: ProductService,
	    private productApi: ProductApi
	) {
		this.options = [
			{
				title: OPTION_SELECT,
				handler: () => {
					let index = this.galleryUploadStorage.getIndex(this.imagePackage);

					this.productApi.selectGalleryImage(this.product.id, index)
						.subscribe((product: Product) => {
							this.galleryUploadStorage.selectImagePackage(this.imagePackage);
						})
				}
			},
			{
				title: OPTION_REMOVE,
				handler: () => {
					let index = this.galleryUploadStorage.getIndex(this.imagePackage);

					this.productApi.removeGalleryImage(this.product.id, index)
						.subscribe((product: Product) => {
							this.galleryUploadStorage.removeImagePackage(this.imagePackage);
						})
				}
			}
		];
	}

	/**
	 * Get full image path of current ImagePackage
	 * 
	 * @returns {string}
	 */
	getFullPath() {
		return this.productService.getImagePackageUrl(this.imagePackage, IMAGE_SIZE);
	}

	/**
	 * When drop another ImagePackage to current
	 * 
	 * @param event
	 */
	onDrop(event: any) {
		let sourceImagePackage = event.dragData;
		let targetImagePackage = this.imagePackage;

		let sourceIndex = this.galleryUploadStorage.getIndex(sourceImagePackage);
		let targetIndex = this.galleryUploadStorage.getIndex(targetImagePackage);
		
		this.productApi.moveGalleryImage(this.product.id, {
			source: sourceIndex,
			target: targetIndex
		})
			.subscribe(() => {
				this.galleryUploadStorage.moveImagePackage(sourceImagePackage, targetImagePackage);
			});
	}

	@ViewChild('productImageMenu') public productImageMenu: ContextMenuComponent;
}
import { Component, OnInit, OnDestroy} from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { ActivatedRoute } from "@angular/router";
import { PriceRate } from "../../../../../../core/models/price-rate.model";
import { ProductService } from "../../../../../../core/services/product.service";
import { PriceRateApi } from "../../../../../../core/api/price-rate.api";
import { Product } from "../../../../../../core/models/product.model";
import { CollectionResponseInterface } from "../../../../../../core/interfaces/system/collection-response.interface";
import { PriceRateData } from "../../../../../../core/request-data/price-rate.data";

const PARAM_ID = 'id';

@Component({
	selector: 'dsh-product-price',
	templateUrl: './price.component.html',
	styleUrls: ['./price.component.css']
})
export class PriceComponent implements OnInit, OnDestroy {

	/**
	 * PriceRate which is used for edit data
	 *
	 * @type {PriceRate}
	 */
	editablePriceRate: PriceRate = new PriceRate();

	/**
	 * Editable PriceRate index (needs for updating info in list)
	 *
	 * @type {null}
	 */
	editableIndex: number = null;

	/**
	 * Form visible indicator
	 *
	 * @type {boolean}
	 */
	isFormVisible: boolean = false;

	/**
	 * PriceRates
	 *
	 * @type {null}
	 */
	priceRates: PriceRate[] = null;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * Current Product
	 *
	 * @type {Product}
	 */
	product: Product = new Product();

	constructor(
		private priceRateApi: PriceRateApi,
		private productService: ProductService,
		private route: ActivatedRoute
	) { }

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {

		this.subscriptions$.push(
			/**
			 * Subscribe for id route parameter
			 */
			this.route.params
				.subscribe(
					(params) => {
						if (params[PARAM_ID]) {
							this.priceRateApi.findAll(params[PARAM_ID])
								.subscribe(
									(response: CollectionResponseInterface<PriceRate>) => {
										this.priceRates = response.data;
									}
								);
						}
					}
				),
			/**
			 * Subscribe for current Product
			 */
			this.productService.getCurrentProductObservable()
				.subscribe((product: Product) => {
					this.product = product;
				})
		);
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => subscription.unsubscribe()
		);
	}

	/**
	 * Show form
	 */
	showForm() {
		this.isFormVisible = true;
	}

	/**
	 * Hide form
	 */
	hideForm() {
		this.isFormVisible = false;
		this.editablePriceRate = new PriceRate();
	}

	/**
	 * Get PriceRate string
	 *
	 * @param priceRate
	 * @returns {string}
	 */
	getPriceRateString(priceRate: PriceRate): string {
		var str =  priceRate.range + ' - ' + priceRate.price + ' грн';

		if (priceRate.postfix) {
			str += '/' + priceRate.postfix
		}

		return str;
	}

	/**
	 * Add PriceRate
	 */
	addPriceRate() {
		let data: PriceRateData = {
			range: this.editablePriceRate.range,
			price: this.editablePriceRate.price,
			postfix: this.editablePriceRate.postfix
		};

		this.priceRateApi.create(this.product, data)
			.subscribe((priceRate: PriceRate) => {
				console.log(this.priceRates);
				this.priceRates.push(Object.assign({}, priceRate));
				this.editablePriceRate = new PriceRate();
			});
	}

	/**
	 * Edit PriceRate
	 *
	 * @param priceRate
	 */
	editPriceRate(priceRate: PriceRate) {
		this.editablePriceRate = Object.assign({}, priceRate);
		this.editableIndex = this.priceRates.indexOf(priceRate);
		this.showForm();
	}

	/**
	 * Save PriceRate
	 */
	savePriceRate() {
		let data: PriceRateData = {
			range: this.editablePriceRate.range,
			price: this.editablePriceRate.price,
			postfix: this.editablePriceRate.postfix
		};

		this.priceRateApi.update(this.product, this.editablePriceRate.id, data)
			.subscribe((priceRate: PriceRate) => {
				this.priceRates[this.editableIndex] = Object.assign(
					this.priceRates[this.editableIndex], priceRate
				);
				this.editableIndex = null;
				this.hideForm()
			});
	}

	/**
	 * Remove PriceRate
	 *
	 * @param priceRate
	 */
	removePriceRate(priceRate: PriceRate) {

		this.priceRateApi.remove(this.product, priceRate.id)
			.subscribe(() => {
				var index = this.priceRates.indexOf(priceRate);
				this.priceRates.splice(index, 1);
			})
	}
}

import { Component } from "@angular/core";
import { ProductService } from "../../../../../core/services/product.service";

@Component({
	selector: 'dsh-product-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.css']
})
export class EditComponent { }
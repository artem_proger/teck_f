import { Component, OnDestroy, OnInit, ElementRef } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { ActivatedRoute, Router } from "@angular/router";
import { interval } from "rxjs/observable/interval";
import { fromEvent } from "rxjs/observable/fromEvent";
import { Product } from "../../../../../../core/models/product.model";
import { Category } from "../../../../../../core/models/category.model";
import { CategoryApi } from "../../../../../../core/api/category.api";
import { ProductService } from "../../../../../../core/services/product.service";
import { SystemMessageService } from "../../../../../../core/services/system/system-message.service";
import { ProductApi } from "../../../../../../core/api/product.api";
import { AppConfig } from "../../../../../../app.config";
import { ProductData } from "../../../../../../core/request-data/product.data";
import { SuccessMessage } from "../../../../../../core/classes/success-message.class";
import { CollectionResponseInterface } from "../../../../../../core/interfaces/system/collection-response.interface";

declare var $: any;

const SUCCESS_MESSAGE = 'Продукт успешно сохранён';
const PARAM_ID = 'id';
const SELECTOR_FORM = 'form';

@Component({
	selector: 'dsh-product-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.css']
})
export class ProductFormComponent implements OnInit, OnDestroy {

	/**
	 * Current product
	 *
	 * @type {Product}
	 */
	product: Product = new Product();

	/**
	 * Categories which have no children
	 *
	 * @type {Array}
	 */
	categories: Category[] = [];

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * Submit form source
	 */
	submitFormSource: any;

	/**
	 * Route params
	 */
	params: any = {};

	constructor(
		private categoryApi: CategoryApi,
	    private productApi: ProductApi,
	    private productService: ProductService,
	    private route: ActivatedRoute,
	    private router: Router,
		private systemMessageService: SystemMessageService,
		private el: ElementRef
	) { }

	ngOnInit() {
		this.submitFormSource = fromEvent($(this.el.nativeElement).find(SELECTOR_FORM)[0], 'submit');
		this.subscriptions$.push(
			/**
			 * Initialize Product and Product's Category of this component
			 * depend on route id parameter
			 */
			this.route.params
				.subscribe(
					(params) => {
						this.params.id = params[PARAM_ID];

						if (this.params.id) {
							this.subscriptions$.push(
								this.productApi.find(this.params.id)
									.subscribe(
										(product: Product) => {
											this.product = product;
											this.productService.setProduct(this.product);
										}
									)
							);
						} else {
							this.product.category = this.categories[0];
							this.productService.setProduct(this.product);
						}
					}
				),
			/**
			 * Subscribe for form submit
			 */
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					var data: ProductData = {
						title: this.product.title,
						description: this.product.description,
						in_store: this.product.in_store,
						category_id: this.product.category.id
					};

					(
						this.isEdit()
							? this.productApi.update(this.product.id, data)
							: this.productApi.create(data)
					)
						.subscribe(
							(data: Product) => {
								if (!this.isEdit()) {
									Object.assign(this.product, data);
									this.router.navigate([
										ProductService.ROUTE_EDIT(this.product.id)
									]);
								} else {
									Object.assign(this.product, data);
								}

								this.systemMessageService.setSystemMessage(
									SuccessMessage.create(SUCCESS_MESSAGE)
								);
							}
						)
				})
		);


		/**
		 * Find all categories which haven't children
		 */
		let findAllParams = {};
		findAllParams[CategoryApi.PARAM_HAS_CHILDREN] = 0;

		this.categoryApi.findAll(findAllParams)
			.subscribe((response: CollectionResponseInterface<Category>) => {
				this.categories = response.data;
				if (!this.product.category || !this.product.category.id) {
					this.product.category = this.categories[0];
				}

				this.productService.setProduct(this.product);
			})
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => subscription.unsubscribe()
		)
	}

	/**
	 * Check are we edit or add product
	 *
	 * @returns {boolean}
	 */
	isEdit() {
		return !!this.product.id;
	}

	/**
	 * Comparator function for Categories select
	 *
	 * @param c1
	 * @param c2
	 * 
	 * @returns {boolean}
	 */
	categoryComparator(c1: Category, c2: Category) {
		if (!c1 || !c2) return false;

		return c1.id === c2.id;
	}
}
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProductRoutingModule } from "./product-routing.module";
import { ProductComponent } from "./product.component";
import { EditComponent } from "./edit/edit.component";
import { ProductFormComponent } from "./edit/form/form.component";
import { PriceComponent } from "./edit/price/price.component";
import { FormsModule } from "@angular/forms";
import { ProductListComponent } from "./list/list.component";
import { ProductFilterComponent } from "./list/filter/filter.component";
import { OrderModule } from "ngx-order-pipe";
import { ProductFilterFieldComponent } from "./list/filter/filter-field/filter-field.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GalleryUploadComponent } from "./edit/gallery-upload/gallery-upload.component";
import { GalleryUploadItemComponent } from "./edit/gallery-upload/gallery-upload-item/gallery-upload-item.component";
import { NgDragDropModule } from "ng-drag-drop/index";
import { ContextMenuModule } from "ngx-contextmenu/lib/ngx-contextmenu";
import { SharedModule } from "../../../../shared/shared.module";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ProductRoutingModule,
		SharedModule,
		OrderModule,
		NgbModule,
		NgDragDropModule.forRoot(),
		ContextMenuModule
	],
	declarations: [
		ProductComponent,
		EditComponent,
		ProductFormComponent,
		GalleryUploadComponent,
		GalleryUploadItemComponent,
		PriceComponent,
		ProductListComponent,
		ProductFilterComponent,
		ProductFilterFieldComponent
	]
})
export class ProductModule { }

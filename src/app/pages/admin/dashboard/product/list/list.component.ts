import { Component, OnInit } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { Meta } from "../../../../../core/interfaces/system/meta.interface";
import { ProductFilter } from "../../../../../core/classes/filter.class";
import { Product } from "../../../../../core/models/product.model";
import { ProductService } from "../../../../../core/services/product.service";
import { ProductFetcher } from "../../../../../core/services/product-fetcher.service";
import { QueryParamService } from "../../../../../core/services/system/query-param.service";
import { ProductApi } from "../../../../../core/api/product.api";
import { PaginatedResponseInterface } from "../../../../../core/interfaces/system/paginated-response.interface";

const PRICE_FROM: number = 200;
const PRICE_TO: number = 800;
const IMAGE_SIZE: string = ProductService.SIZE_MINI;

@Component({
	selector: 'dsh-product-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})
export class ProductListComponent implements OnInit {

	/**
	 * Products meta data
	 */
	meta: Meta;

	/**
	 * Products
	 * @type {Array}
	 */
	products: Product[] = [];

	/**
	 * ProductFilter
	 */
	filter: ProductFilter;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(
		private productApi: ProductApi,
		private productFetcher: ProductFetcher,
	    private productService: ProductService,
		private queryParam: QueryParamService
	) {

		/**
		 * Subscribe for fetching products response
		 */
		this.subscriptions$.push(
			this.productFetcher.getObservable()
				.subscribe(
					(response: PaginatedResponseInterface<Product>) => {
						this.meta = response.meta;
						this.products = response.data;
					}
				)
		);

		/**
		 * Current query params
		 *
		 * @type {any}
		 */
		var currentParams = this.queryParam.getCurrentParams();

		/**
		 * Create ProductFilter
		 *
		 * @type {ProductFilter}
		 */
		this.filter = ProductFilter.create(
			currentParams,
			{
				from: PRICE_FROM,
				to: PRICE_TO
			}
		);

		/**
		 * Fetch Products
		 */
		this.productFetcher.applyParams(currentParams);
	}

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {
		this.productFetcher.fetch();
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => {
				subscription.unsubscribe();
			}
		);
		this.productFetcher.clearFilter();
		this.queryParam.clearQueryString();
	}

	/**
	 * Checks is Product hasn't price
	 *
	 * @param product
	 * @returns {boolean}
	 */
	isEmptyPrice(product: Product) {
		return (product.price_from == 0) && (product.price_to == 0);
	}

	/**
	 * Get Product thumbnail URL
	 *
	 * @param product
	 *
	 * @returns {undefined|string}
	 */
	getProductImage(product: Product) {
		return this.productService.getImageUrl(product, IMAGE_SIZE)
	}

	/**
	 * Change page (pagination)
	 *
	 * @param page
	 */
	pageChange(page: number) {
		let params = { page: page };
		this.productFetcher.applyParams(params);
		this.queryParam.applyParams(params, true);
		this.productFetcher.fetch();
	}

	/**
	 * Remove Product
	 *
	 * @param product
	 */
	removeProduct(product: Product) {
		this.productApi.remove(product.id)
			.subscribe(() => {
				this.productFetcher.fetch()
			});
	}

	/**
	 * Filter applying handler
	 *
	 * @param filter
	 */
	onFilterApply(filter: ProductFilter) {
		var filterParams = filter.getParamsObj();
		this.productFetcher.clearFilter();
		this.productFetcher.applyParams(filterParams);
		this.queryParam.applyParams(filterParams);
		this.productFetcher.fetch();

	}
}

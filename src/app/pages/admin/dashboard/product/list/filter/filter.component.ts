import { Component, ElementRef, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { fromEvent } from "rxjs/observable/fromEvent";
import { interval } from "rxjs/observable/interval";
import { ProductFilter } from "../../../../../../core/classes/filter.class";
import { QueryParamService } from "../../../../../../core/services/system/query-param.service";
import { Category } from "../../../../../../core/models/category.model";
import { CategoryApi } from "../../../../../../core/api/category.api";
import { CollectionResponseInterface } from "../../../../../../core/interfaces/system/collection-response.interface";
import { AppConfig } from "../../../../../../app.config";

declare var $: any;

const SELECTOR_APPLY_FILTER_BUTTON = '#apply-filter-btn';
const SELECTOR_PRICE_INPUT = '#price';

@Component({
	selector: 'dsh-product-filter',
	templateUrl: './filter.component.html',
	styleUrls: ['./filter.component.css'],
	providers: [QueryParamService]
})
export class ProductFilterComponent implements OnInit {

	/**
	 * Current ProductFilter
	 */
	@Input()
	filter: ProductFilter;

	/**
	 * Apply filter EventEmitter
	 *
	 * @type {EventEmitter<ProductFilter>}
	 */
	@Output()
	filterApply: EventEmitter<ProductFilter> = new EventEmitter<ProductFilter>();

	/**
	 * All leaf Categories
	 *
	 * @type {Array}
	 */
	categories: Category[] = [];

	/**
	 * Price filter input
	 */
	priceInput: any;

	/**
	 * Submit form source
	 */
	submitFormSource: any;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(
		private el: ElementRef,
		private categoryApi: CategoryApi
	) { }

	/**
	 * Life cycle hooks
	 */
	ngOnInit() {

		this.submitFormSource = fromEvent(
			$(this.el.nativeElement).find(SELECTOR_APPLY_FILTER_BUTTON)[0], 'click'
		);

		/**
		 * Fetch all leaf categories
		 */
		let findAllParams = {};
		findAllParams[CategoryApi.PARAM_HAS_CHILDREN] = 0;

		this.categoryApi.findAll(findAllParams)
			.subscribe((response: CollectionResponseInterface<Category>) => {
				this.categories = response.data;
				this.filter.category_id.value = this.categories.length > 0
					? this.categories[0].id
					: undefined;
			});

		/**
		 * Subscribe & throttle apply filter button
		 */
		this.subscriptions$.push(
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					this.filter.in_store.value = (this.filter.in_store.value)
						? 1
						: 0;

					this.filterApply.emit(this.filter);
				})
		);

		/**
		 * Init range on filter price input
		 */
		this.priceInput = $(this.el.nativeElement).find(SELECTOR_PRICE_INPUT);
		this.priceInput.ionRangeSlider({
			type: "double",
			min: 100,
			max: 1000,
			from: this.filter.price.value.from,
			to: this.filter.price.value.to,
			grid: true,
			grid_snap: true,
			onChange: (data: any) => {
				this.filter.price.value.from = data.from;
				this.filter.price.value.to = data.to;
			}
		});
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => {
				subscription.unsubscribe();
			}
		)
	}

	/**
	 * Reset Filter
	 */
	reset() {
		this.filter.reset();
	}
}
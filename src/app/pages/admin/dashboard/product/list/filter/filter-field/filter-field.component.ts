import { Component, Input } from "@angular/core";
import { FilterField } from "../../../../../../../core/classes/filter-field.class";

@Component({
	selector: 'product-filter-field',
	templateUrl: './filter-field.component.html'
})
export class ProductFilterFieldComponent {
	@Input() title: string;
	@Input() filterField: FilterField<any>;

	/**
	 * Enable or disable field in filter
	 */
	toggleFilterField() {
		this.filterField.selected = !this.filterField.selected;
	}
}
import { Component, ElementRef } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { fromEvent } from "rxjs/observable/fromEvent";
import { interval } from "rxjs/observable/interval";
import { ActivatedRoute, Router } from "@angular/router";
import { SettingsService } from "../../../../../core/services/settings.service";
import { SettingsApi } from "../../../../../core/api/settings.api";
import { SystemMessageService } from "../../../../../core/services/system/system-message.service";
import { PageService } from "../../../../../core/services/page.service";
import { AppConfig } from "../../../../../app.config";
import { SuccessMessage } from "../../../../../core/classes/success-message.class";

declare var $: any;

const SELECTOR_FORM = 'form';
const PARAM_PAGE = 'page';
const MESSAGE_SUCCESS = 'Страница сохранена';

@Component({
	selector: 'dsh-page',
	templateUrl: './page.component.html'
})
export class PageComponent {

	editorOptions: any = {
		heightMin: 300
	};

	content: string = '';
	subscriptions$: Subscription[] = [];
	submitFormSource: any;
	settingsMapping: any;
	pageKey: any;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private settingsService: SettingsService,
		private settingsApi: SettingsApi,
		private systemMessageService: SystemMessageService,
		private pageService: PageService,
		private el: ElementRef
	) {
		this.settingsMapping = this.settingsService.getNameMapping();
	}

	ngOnInit() {
		this.submitFormSource = fromEvent($(this.el.nativeElement)
			.find(SELECTOR_FORM)[0], 'submit');

		this.subscriptions$.push(
			this.route.params
				.subscribe(
					(params) => {
						this.pageKey = this.pageService.getSettingKeyByParam(
							params[PARAM_PAGE]
						);

						if (!this.pageKey) {
							this.router.navigate([AppConfig.ROUTE_404]);
						}

						let findAllParams = {};
						findAllParams[SettingsApi.PARAM_IN] = [
							this.pageKey
						].join(',');

						this.settingsApi.findAll(findAllParams)
							.subscribe((response: any) => {
								this.content = response[this.pageKey]
							});
					}
				)
		);

		this.subscriptions$.push(
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					var data = {};
					data[this.pageKey] = this.content;

					this.settingsApi.save(data)
						.subscribe((response: any) => {
							this.systemMessageService.setSystemMessage(
								SuccessMessage.create(MESSAGE_SUCCESS)
							);
							this.content = response[this.pageKey];
						})
				})
		)
	}

	

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => subscription.unsubscribe()
		)
	}
}

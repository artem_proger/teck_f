import { NgModule } from "@angular/core";
import { ListComponent } from "./slider/list/list.component";
import { FormComponent } from "./slider/form/form.component";
import { SliderComponent } from "./slider/slider.component";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { SettingsRoutingModule } from "./settings-routing.module";
import { GeneralComponent } from "./general/general.component";
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { PageComponent } from "./page/page.component";
import { SharedModule } from "../../../../shared/shared.module";
import { SliderApi } from "../../../../core/api/slider.api";
import { SliderService } from "../../../../core/services/slider.service";
import { SettingsApi } from "../../../../core/api/settings.api";
import { SettingsService } from "../../../../core/services/settings.service";
import { PageService } from "../../../../core/services/page.service";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,
		SettingsRoutingModule,
		FroalaEditorModule.forRoot(),
		FroalaViewModule.forRoot()
	],
	declarations: [
		SliderComponent,
		FormComponent,
		ListComponent,
		GeneralComponent,
		PageComponent
	]
})
export class SettingsModule { }

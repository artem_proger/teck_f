import { Component, ElementRef } from "@angular/core";
import { interval } from "rxjs/observable/interval";
import { Subscription } from "rxjs/Rx";
import { fromEvent } from "rxjs/observable/fromEvent";
import { InputItem } from "../../../../../core/classes/input-item.class";
import { SettingsService } from "../../../../../core/services/settings.service";
import { SettingsApi } from "../../../../../core/api/settings.api";
import { SystemMessageService } from "../../../../../core/services/system/system-message.service";
import { AppConfig } from "../../../../../app.config";
import { SuccessMessage } from "../../../../../core/classes/success-message.class";

declare var $: any;

const SELECTOR_FORM = 'form';
const MESSAGE_SUCCESS = 'Настройки успешно сохранены';

@Component({
	selector: 'dsh-general-settings',
	templateUrl: './general.component.html'
})
export class GeneralComponent {

	/**
	 * Settings
	 * 
	 * @type {{}}
	 */
	settings: any = {};

	/**
	 * Phones InputItems
	 */
	phonesInputItems: InputItem[];

	/**
	 * Schedule InputItems
	 */
	scheduleInputItems: InputItem[];

	/**
	 * Submit form source
	 */
	submitFormSource: any;

	/**
	 * Component subscriptions
	 * 
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * All settings keys
	 */
	settingsKeys: any;

	constructor(
		private el: ElementRef,
	    private settingsApi: SettingsApi,
	    private settingsService: SettingsService,
		private systemMessageService: SystemMessageService
	) {
		this.settingsKeys = settingsService.getNameMapping();
	}

	/**
	 * Life cycle hooks
	 */
	ngOnInit() {
		this.submitFormSource = fromEvent($(this.el.nativeElement)
			.find(SELECTOR_FORM)[0], 'submit');

		/**
		 * Subscribe for form submit
		 */
		this.subscriptions$.push(
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					var data = {
						city: this.settings.city,
						zip: this.settings.zip,
						street: this.settings.street,
						email: this.settings.email,
						site_name: this.settings.site_name,
						phones: this.phonesInputItems.map((phoneInputItem: InputItem) => {
							return phoneInputItem.value;
						}),
						schedule: this.scheduleInputItems.map((scheduleInputItem: InputItem) => {
							return scheduleInputItem.value;
						})
					};

					this.settingsApi.save(data)
						.subscribe((response: any) => {
							this.settings = Object.assign(this.settings, response);
							this.initInputItems(this.settings);
							this.systemMessageService.setSystemMessage(
								SuccessMessage.create(MESSAGE_SUCCESS)
							)
						});
				})
		);

		var findAllParams = {};
		findAllParams[SettingsApi.PARAM_IN] = this.settingsService.getGeneralSettings().join(',');

		/**
		 * Get all settings
		 */
		this.settingsApi.findAll(findAllParams)
			.subscribe((response: any) => {
				this.settings = response;
				this.initInputItems(this.settings);
			})
	}

	/**
	 * Init InputItems by settings
	 * @param settings
	 */
	initInputItems(settings: any) {

		this.phonesInputItems = settings.phones
			? this.settings.phones.map((phone: string) => {
			return new InputItem(phone);
		})
			: [];

		this.scheduleInputItems = settings.schedule
			? this.settings.schedule.map((scheduleItem: string) => {
			return new InputItem(scheduleItem);
		})
			: [];
	}
}

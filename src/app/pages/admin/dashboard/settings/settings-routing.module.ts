import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SliderComponent } from "./slider/slider.component";
import { FormComponent } from "./slider/form/form.component";
import { GeneralComponent } from "./general/general.component";
import { PageComponent } from "./page/page.component";

const settingsRoutes: Routes = [
	{
		path: '',
		redirectTo: 'slider',
		pathMatch: 'full',
	},
	{
		path: 'slider',
		component: SliderComponent,
		data: {title: 'Слайдер'},
		children: [
			{
				path: 'add',
				component: FormComponent,
				data: {title: 'Добавить слайд'}
			},
			{
				path: 'edit/:id',
				component: FormComponent,
				data: {title: 'Редактировать слайд'}
			}
		]
	},
	{
		path: 'general',
		component: GeneralComponent,
		data: {title: 'Настройки'}
	},
	{
		path: 'page/:page',
		component: PageComponent,
		data: {title: 'Редактировать страницу'}
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(settingsRoutes)
	],
	exports: [
		RouterModule
	]
})
export class SettingsRoutingModule { }

import { Component, Input, ElementRef } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { Router, ActivatedRoute } from "@angular/router";
import { fromEvent } from "rxjs/observable/fromEvent";
import { interval } from "rxjs/observable/interval";
import { Slide } from "../../../../../../core/models/slide.model";
import { SliderApi } from "../../../../../../core/api/slider.api";
import { SystemMessageService } from "../../../../../../core/services/system/system-message.service";
import { SliderService } from "../../../../../../core/services/slider.service";
import { SlideData } from "../../../../../../core/request-data/slide.data";
import { AppConfig } from "../../../../../../app.config";
import { SuccessMessage } from "../../../../../../core/classes/success-message.class";
import { ErrorMessage } from "../../../../../../core/classes/error-message.class";

declare var $: any;

const SELECTOR_FORM = 'form';
const PARAM_ID = 'id';
const MESSAGE_SLIDE_SUCCESS = 'Слайд успешно сохранён';
const MESSAGE_IMAGE_SUCCESS = 'Изображение слайда успешно обновлено';
const REQUEST_PARAM_IMAGE = 'image';

@Component({
	selector: 'slider-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.css']
})
export class FormComponent {

	/**
	 * Current Slide
	 */
	@Input()
	slide: Slide;

	/**
	 * Component Subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * Submit form source
	 */
	submitFormSource: any;

	constructor(
		private sliderService: SliderService,
	    private sliderApi: SliderApi,
		private route: ActivatedRoute,
		private router: Router,
	    private systemMessageService: SystemMessageService,
		private el: ElementRef
	) { }

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {
		this.submitFormSource = fromEvent($(this.el.nativeElement)
			.find(SELECTOR_FORM)[0], 'submit');

		this.subscriptions$.push(
			/**
			 * Subscribe for id parameter
			 */
			this.route.params.subscribe(
				(params) => {
					if (params[PARAM_ID] && !this.isEdit()) {
						this.sliderApi.find(params[PARAM_ID])
							.subscribe((slide: Slide) => {
								this.slide = slide;
							})
					}
				}
			),

			/**
			 * Subscribe on current Slide
			 */
			this.sliderService.getCurrentSlideObservable()
				.subscribe((slide: Slide) => {
					if (!slide) return;
					this.slide = Object.assign(new Slide(), slide)
				}),

			/**
			 * Subscribe on removed Slide
			 */
			this.sliderService.getRemovedSlideObservable()
				.subscribe((slide: Slide) => {
					if (!slide) return;
					if (this.slide.id === slide.id) {
						this.router.navigate([SliderService.ROUTE_HOME]);
					}
				}),

			/**
			 * Subscribe on form submit
			 */
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					var data: SlideData = {
						title: this.slide.title,
						text: this.slide.text,
						position: this.slide.position
					};

					(
						this.isEdit()
							? this.sliderApi.update(this.slide.id, data)
							: this.sliderApi.create(data)
					)
						.subscribe((response: Slide) => {

							if (this.isEdit()) {
								this.slide = Object.assign(this.slide, response);
								this.sliderService.editSlide(this.slide);
								this.systemMessageService.setSystemMessage(
									SuccessMessage.create(MESSAGE_SLIDE_SUCCESS)
								);
							} else {
								this.slide = Object.assign(this.slide, response);
								this.sliderService.addSlide(this.slide);
								this.sliderService.selectSlide(this.slide);
								this.router.navigate([SliderService.ROUTE_EDIT(this.slide.id)]);
							}
						})
				})
		);

		this.slide = new Slide();
	}

	ngOnDestroy() {
		this.subscriptions$.forEach((subscription: Subscription) => {
			subscription.unsubscribe()
		});
	}

	/**
	 * Is edit slide
	 * 
	 * @returns {Slide|boolean}
	 */
	isEdit() {
		return this.slide && !!this.slide.id;
	}

	/**
	 * Get thumbnail URL
	 * 
	 * @returns {null|string}
	 */
	getImageUrl() {
		return this.sliderService.getImageUrl(
			this.slide,
			SliderService.SIZE_MINI
		);
	}

	/**
	 * On change upload image input
	 *
	 * @param input
	 */
	onImageInputChange(input: any) {

		if (input.files.length == 0) {
			return;
		}

		var imageFile = input.files[0];

		var formData = new FormData();
		formData.append(REQUEST_PARAM_IMAGE, imageFile);
		
		this.sliderApi.setImage(this.slide.id, formData)
			.subscribe(
				(response: Slide) => {
					this.slide.image = response.image;
					this.sliderService.editSlide(this.slide);
					this.systemMessageService.setSystemMessage(
						SuccessMessage.create(MESSAGE_IMAGE_SUCCESS)
					);
				},
				err => {
					this.systemMessageService.setSystemMessage(
						ErrorMessage.create(err.error)
					);
					input.value = "";
				}
			);
	}
}
import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { Slide } from "../../../../../../core/models/slide.model";
import { SliderService } from "../../../../../../core/services/slider.service";
import { SliderApi } from "../../../../../../core/api/slider.api";

@Component({
	selector: 'slider-items',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})
export class ListComponent {

	/**
	 * Slides
	 */
	@Input() 
	slides: Slide[];

	/**
	 * Action process indicator
	 *
	 * @type {boolean}
	 */
	processAction: boolean = false;

	constructor(
		private sliderService: SliderService,
		private sliderApi: SliderApi,
		private router: Router
	) { }

	/**
	 * Select Slide
	 *
	 * @param slide
	 */
	selectSlide(slide: Slide) {
		if (this.processAction) return;
		this.processAction = true;
		this.sliderService.selectSlide(slide);
		this.router.navigate([SliderService.ROUTE_EDIT(slide.id)]);
		this.processAction = false;
	}

	/**
	 * Get Slide image URL
	 * @param slide
	 * @returns {null|string}
	 */
	getSlideImage(slide: Slide) {
		return this.sliderService.getImageUrl(slide, SliderService.SIZE_MINI);
	}

	/**
	 * Remove Slide
	 *
	 * @param slide
	 */
	removeSlide(slide: Slide) {
		if (this.processAction) return;
		this.processAction = true;
		this.sliderApi.remove(slide.id)
			.subscribe((response: any) => {
				console.log(response);
				var index = this.slides.indexOf(slide);
				this.slides.splice(index, 1);
				this.sliderService.removeSlide(slide);
				this.processAction = false;
			})
	}
}

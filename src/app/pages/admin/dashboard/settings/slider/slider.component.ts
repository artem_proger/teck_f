import { Component } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { Slide } from "../../../../../core/models/slide.model";
import { SliderApi } from "../../../../../core/api/slider.api";
import { SliderService } from "../../../../../core/services/slider.service";
import { CollectionResponseInterface } from "../../../../../core/interfaces/system/collection-response.interface";

@Component({
	selector: 'dsh-slider',
	templateUrl: './slider.component.html',
	styleUrls: ['./slider.component.css']
})
export class SliderComponent {

	/**
	 * Shared Slides
	 *
	 * @type {Array}
	 */
	slides: Slide[] = [];

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];
	
	constructor(
		private sliderApi: SliderApi,
	    private sliderService: SliderService
	) {

		/**
		 * Find all Slides
		 */
		this.sliderApi.findAll()
			.subscribe((response: CollectionResponseInterface<Slide>) => {
				this.slides = response.data;
			});

		this.subscriptions$.push(
			/**
			 * Subscribe for added Slide
			 */
			this.sliderService.getAddedSlideObservable()
				.subscribe((slide: Slide) => {
					if (!slide) return;
					this.slides.push(slide);
				}),
			/**
			 * Subscribe for edit Slide
			 */
			this.sliderService.getEditedSlideObservable()
				.subscribe((slide: Slide) => {
					if (!slide) return;

					var oldSlide = this.slides.filter((item: Slide) => {
						return item.id === slide.id
					})[0];

					var index = this.slides.indexOf(oldSlide);

					this.slides[index] = slide;
				})
		);
	}

	/**
	 * Get add Slider route
	 *
	 * @returns {string}
	 */
	getAddSliderRoute() {
		return SliderService.ROUTE_ADD;
	}
}
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard.component";

const dashboardRoutes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		data: { title: 'Dashboard Welcome!'},
		children: [
			{
				path: 'categories',
				loadChildren: './category/category.module#CategoryModule',
				//canActivate: [AuthGuard]
			},
			{
				path: 'products',
				loadChildren: './product/product.module#ProductModule'
			},
			{
				path: 'settings',
				loadChildren: './settings/settings.module#SettingsModule'
			}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(dashboardRoutes)],
	exports: [RouterModule]
})
export class DashboardRoutingModule { }
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs/Rx";
import { NotificationsService } from "angular2-notifications";
import { SystemTitle } from "../../../core/services/system/system-title.service";
import { SystemMessageService } from "../../../core/services/system/system-message.service";
import { SystemMessage } from "../../../core/classes/system-message.class";

declare var $ :any;

const DEFAULT_TITLE = 'Админ панель';

@Component({
	selector: 'admin-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: [
		'./dashboard.component.css'
	]
})
export class DashboardComponent implements OnInit, OnDestroy {

	/**
	 * General title
	 *
	 * @type {string}
	 */
	title: string = DEFAULT_TITLE;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(
		private systemTitle: SystemTitle,
		private systemMessageService: SystemMessageService,
		private notificationService: NotificationsService
	) { }

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {
		this.subscriptions$.push(
			/**
			 * System messages subscriptions
			 */
			this.systemMessageService.getSystemMessageObservable()
				.subscribe((systemMessage: SystemMessage) => {
					if (systemMessage) {
						this.notificationService.success(systemMessage.message);
					}
				}),
			/**
			 * Subscribe for actual general title
			 */
			this.systemTitle.getTitleObservable()
				.subscribe(title => this.title = title)
		);
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(function (subscription: Subscription) {
			subscription.unsubscribe();
		});
	}
}

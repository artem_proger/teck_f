import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CategoryComponent } from "./category.component";
import { CategoryRoutingModule } from "./category-routing.module";
import { TreeComponent } from "./tree/tree.component";
import { FormComponent } from "./form/form.component";
import { ContextMenuModule } from "ngx-contextmenu/lib/ngx-contextmenu";
import { TreeItemComponent } from "./tree/tree-item/tree-item.component";
import { TreeCollectionComponent } from "./tree/tree-collection/tree-collection.component";
import { NgDragDropModule } from "ng-drag-drop/index";
import { OrderModule } from "ngx-order-pipe";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../../../../shared/shared.module";

@NgModule({
	imports: [
		CommonModule,
		CategoryRoutingModule,
		SharedModule,
		ContextMenuModule,
		OrderModule,
		FormsModule,
		NgDragDropModule.forRoot()
	],
	declarations: [
		CategoryComponent,
		TreeComponent,
		TreeItemComponent,
		TreeCollectionComponent,
		FormComponent
	],
	exports: [
		CategoryComponent,
		TreeComponent,
		FormComponent
	]
})
export class CategoryModule { }

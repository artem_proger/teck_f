import { Component } from "@angular/core";
import { CategoryTree } from "../../../../core/classes/category-tree.class";

@Component({
	selector: 'dsh-category',
	templateUrl: './category.component.html',
	styleUrls: ['./category.component.css'],
	providers: [
		CategoryTree
	]
})
export class CategoryComponent {
	constructor(
		private categoryTree: CategoryTree
	) { }
}
import { Component, ViewChild } from "@angular/core";
import { ContextMenuComponent } from "ngx-contextmenu/lib/contextMenu.component";
import { Router } from "@angular/router";
import { DropEvent } from "ng-drag-drop/index";
import { ContextMenuOption } from "../../../../../core/classes/context-menu-option.class";
import { Category } from "../../../../../core/models/category.model";
import { CategoryApi } from "../../../../../core/api/category.api";
import { CategoryService } from "../../../../../core/services/category.service";
import { CategoryTree } from "../../../../../core/classes/category-tree.class";
import { CollectionResponseInterface } from "../../../../../core/interfaces/system/collection-response.interface";

const OPTION_CREATE = 'Создать категорию';

@Component({
	selector: 'dsh-category-tree',
	templateUrl: './tree.component.html',
	styleUrls: ['./tree.component.css'],
})
export class TreeComponent {

	/**
	 * Context menu options of empty area of category TreeComponent
	 */
	options: ContextMenuOption[];

	/**
	 * All categories
	 */
	categories: Category[] = null;
	
	constructor(
		private categoryApi: CategoryApi,
		private categoryService: CategoryService,
		private categoryTree: CategoryTree,
	    private router: Router
	) {
		this.options = [
			{
				title: OPTION_CREATE,
				handler: (event: any) => {
					this.categoryService.setCategory(new Category());
					this.router.navigate(
						[CategoryService.ROUTE_ADD]
					)
				}
			}
		];

		/**
		 * Init find all categories request query params
		 */
		let findAllParams = {};
		findAllParams[CategoryApi.PARAM_PARENT_ID] = 0;

		/**
		 * Fetch all categories
		 */
		this.categoryApi.findAll(findAllParams)
			.subscribe(
				(response: CollectionResponseInterface<Category>) => {
					this.categoryTree.setCategories(response.data);
					this.categories = this.categoryTree.getCategories();
				},
				err => console.log(err)
			);
	}

	/**
	 * When drop draggable category to empty area
	 * @param event
	 */
	onDrop(event: DropEvent) {
		let draggable = event.dragData;

		if (!this.categories.some(category => category == draggable.category)) {
			this.categoryApi.assign(draggable.category.id, null)
				.subscribe(() => {
					let index = draggable.levelCategories.indexOf(draggable.category);
					draggable.levelCategories.splice(index, 1);
					this.categories.push(draggable.category);
				}, err => console.log(err));
		}
	}

	@ViewChild('generalMenu') public generalMenu: ContextMenuComponent;
}

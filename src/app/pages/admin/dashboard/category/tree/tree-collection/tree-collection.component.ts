import { Component, Input } from "@angular/core";
import { Category } from "../../../../../../core/models/category.model";

@Component({
	selector: 'category-tree-collection',
	templateUrl: './tree-collection.component.html',
	styleUrls: ['./tree-collection.component.css']
})
export class TreeCollectionComponent {
	
	/**
	 * Current categories
	 */
	@Input()
	categories: Category[];

	/**
	 * Parent category for proxying to TreeItemComponent
	 */
	@Input()
	parentCategory: Category;
}
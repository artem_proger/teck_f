import { Component, Input, ViewChild, ElementRef, OnDestroy } from "@angular/core";
import { ContextMenuComponent } from "ngx-contextmenu/lib/contextMenu.component";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import { Category } from "../../../../../../core/models/category.model";
import { ContextMenuOption } from "../../../../../../core/classes/context-menu-option.class";
import { CategoryDraggableEvent } from "../../../../../../core/events/category-draggable.event";
import { CategoryService } from "../../../../../../core/services/category.service";
import { CategoryApi } from "../../../../../../core/api/category.api";
import { CategoryTree } from "../../../../../../core/classes/category-tree.class";

declare var $ :any;

const OPTION_CREATE_CHILD_CATEGORY = 'Создать дочернюю категорию';
const OPTION_REMOVE_CATEGORY = 'Удалить';
const CLASS_DRAGGABLE = 'category';
const PARAM_ID = 'id';

@Component({
	selector: 'category-tree-item',
	templateUrl: './tree-item.component.html',
	styleUrls: ['./tree-item.component.css'],
})
export class TreeItemComponent implements OnDestroy {
	/**
	 * Category
	 */
	@Input() 
	category: Category;
	/**
	 * Parent category
	 */
	@Input()
	parentCategory: Category = null;

	/**
	 * Categories in one level with current category (all children of parent category)
	 * (for removing)
	 */
	@Input()
	levelCategories: Category[];

	/**
	 * Context menu options
	 */
	options: ContextMenuOption[];

	/**
	 * Is current category selected
	 * 
	 * @type {boolean}
	 */
	selected: boolean = false;

	/**
	 * Draggable event data for this item
	 */
	draggable: CategoryDraggableEvent;

	/**
	 * Is enable drop category to this category
	 *
	 * @type {boolean}
	 */
	dropEnabled: boolean = true;

	/**
	 * Class which moved when dragging. Also need for unselecting element (blur)
	 *
	 * @type {string}
	 */
	draggableClass: string = CLASS_DRAGGABLE;

	/**
	 * Is expanded for showing current category children
	 *
	 * @type {boolean}
	 */
	expanded: boolean = false;

	/**
	 *
	 * @type {{}}
	 */
	params: any = {};

	/**
	 * Component subscriptions
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	constructor(
		private elementRef: ElementRef,
		private route: ActivatedRoute,
	    private router: Router,
	    private categoryService: CategoryService,
	    private categoryTree: CategoryTree,
	    private categoryApi: CategoryApi
	) {

		/**
		 * Subscribe for id route parameter
		 */
		this.subscriptions$.push(
			this.route.params.subscribe(
				(params) => {
					this.params.id = params[PARAM_ID];
				}
			)
		);

		/**
		 * Init options
		 *
		 * @type {{title: string, handler: (function(): undefined)}[]}
		 */
		this.options = [
			{
				title: OPTION_CREATE_CHILD_CATEGORY,
				handler: () => {
					this.categoryService.setParentCategory(this.category);
					this.expanded = true;
					this.router.navigate(
						[CategoryService.ROUTE_ADD]
					);
				}
			},
			{
				title: OPTION_REMOVE_CATEGORY,
				handler: () => {
					this.remove();
				}
			}
		];
	}

	/**
	 * Component life cycle hook handlers
	 */

	ngOnInit() {
		this.draggable = {
			category: this.category,
			levelCategories: this.levelCategories
		};
	}

	ngOnDestroy() {
		this.subscriptions$.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}

	/**
	 * Remove current category
	 */
	remove() {
		this.categoryApi.delete(this.category.id)
			.subscribe(() => {
				this.categoryService.removeCategory(this.category);
				this.categoryTree.removeCategory(
					this.category, this.levelCategories
				);
			}, err => console.log(err));
	}

	/**
	 * Get category thumbnail URL
	 *
	 * @returns {string}
	 */
	getThumbnailUrl() {
		return this.categoryService.getThumbnailUrl(
			this.category,
			CategoryService.SIZE_THUMBNAIL
		);
	}

	/**
	 * Draggable event handlers...
	 */

	onRightClick() {
		this.selected = true;
	}

	onBlur() {
		this.selected = false;
	}

	onClick() {
		this.selected = !this.selected;
		if (!this.selected) {
			$(this.elementRef.nativeElement).find('.' + this.draggableClass).blur();
		} else {
			this.categoryService.setCategory(this.category, this.parentCategory);
			this.router.navigate([CategoryService.ROUTE_EDIT(this.category.id)]);
		}
	}

	onDrop(e: any) {
		var draggable = e.dragData;
		if (!this.categoryTree.isChild(draggable.category, this.category)
			&& this.category !== draggable.category) {

			this.categoryApi.assign(draggable.category.id, this.category.id)
				.subscribe(
					(response: Category) => {
						let index = draggable.levelCategories.indexOf(draggable.category);
						draggable.levelCategories.splice(index, 1);
						this.category.children.push(draggable.category);
						this.categoryService.setCategory(draggable.category, this.category);
					},
					err => console.log(err)
				);
		}
		this.expanded = true;
	}

	onDragLeave() {
		this.dropEnabled = true;
	}

	onDragEnd() {
		this.dropEnabled = true;
	}

	onMouseDown() {
		this.dropEnabled = false;
	}

	onMouseUp() {
		this.dropEnabled = true;
	}

	/**
	 * Context menu event handlers
	 */

	onClickContextMenuItem() {
		this.selected = false;
	}


	@ViewChild('categoryMenu') public categoryMenu: ContextMenuComponent;
}


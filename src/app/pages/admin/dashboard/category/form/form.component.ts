import { Component, OnInit, OnDestroy, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import { fromEvent } from "rxjs/observable/fromEvent";
import { interval } from "rxjs/observable/interval";
import { Category } from "../../../../../core/models/category.model";
import { CategoryService } from "../../../../../core/services/category.service";
import { CategoryTree } from "../../../../../core/classes/category-tree.class";
import { CategoryApi } from "../../../../../core/api/category.api";
import { SystemMessageService } from "../../../../../core/services/system/system-message.service";
import { SuccessMessage } from "../../../../../core/classes/success-message.class";
import { CategoryData } from "../../../../../core/request-data/category.data";
import { AppConfig } from "../../../../../app.config";
import { ErrorMessage } from "../../../../../core/classes/error-message.class";
import { CategoryMigrationEvent } from "../../../../../core/events/category-migration.event";

declare var $: any;

const THUMBNAIL_SIZE = CategoryService.SIZE_MEDIUM;
const MESSAGE_CATEGORY_SAVE = 'Категория сохранена';
const MESSAGE_THUMBNAIL_SAVE = 'Миниатюра категории успешно обновлена';
const PARAM_ID = 'id';
const SELECTOR_FORM = 'form';
const REQUEST_PARAM_THUMBNAIL = 'thumbnail';

@Component({
	selector: 'dsh-category-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit, OnDestroy {

	/**
	 * Category model
	 */
	category: Category;

	/**
	 * Parent category
	 */
	parentCategory: Category = null;

	/**
	 * Route params
	 */
	params: any = {};

	/**
	 * Submit form source
	 */
	submitFormSource: any;

	/**
	 * Component subscriptions
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * Form jQuery object
	 */
	form: any;
	

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private categoryService: CategoryService,
		private categoryTree: CategoryTree,
	    private categoryApi: CategoryApi,
	    private systemMessageService: SystemMessageService,
	    private el: ElementRef
	) { }

	ngOnInit() {
		this.submitFormSource = fromEvent($(this.el.nativeElement).find(SELECTOR_FORM)[0], 'submit');
		this.subscriptions$.push(
			/**
			 * Subscribe for id route parameter
			 */
			this.route.params
				.subscribe(
					(params) => {
						this.params.id = params[PARAM_ID];

						if (!this.params.id) {
							this.categoryService.setCategory(new Category);
						}
					}
				),

			/**
			 * Subscribe for removed category. If removed category is current component category -
			 * need to set current category to new Category object
			 */
			this.categoryService.getRemovedCategoryObservable()
				.subscribe(
					(category: Category) => {
						if (this.category.id == category.id) {
							this.router.navigate([CategoryService.ROUTE_ADD]);
						}
					}
				),
			/**
			 * Observe category which user select from category TreeComponent
			 */
			this.categoryService
				.getCurrentCategoryObservable()
				.subscribe(
					(categoryMigration: CategoryMigrationEvent) => {
						this.category = categoryMigration.category;
						this.parentCategory = (categoryMigration.parentCategory)
							? categoryMigration.parentCategory
							: null;

						if (this.params.id && !this.category.id) {
							this.categoryApi.find(this.params[PARAM_ID])
								.subscribe(
									(response: Category) => {
										this.category = this.categoryTree
											.findCategory(response.id);
									},
									err => {
										if (err.status == 404) {
											this.router.navigate([AppConfig.ROUTE_404]);
										}
									}
								);
						}
					}
				),
			/**
			 * Subscribe for form submit
			 */
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					let data: CategoryData = { title: this.category.title };

					if (this.parentCategory) {
						data.parent_id = this.parentCategory.id;
					}

					(
						(this.isEdit())
							? this.categoryApi.update(this.category.id, data)
							: this.categoryApi.create(data)
					)
						.subscribe(
							(data: Category) => {

								if (!this.isEdit()) {
									this.category = Object.assign(this.category, data);
									this.categoryTree.addCategory(
										this.category, this.parentCategory
									);
									this.categoryService.setCategory(
										this.category, this.parentCategory
									);

									this.router.navigate([
										CategoryService.ROUTE_EDIT(this.category.id)
									]);
								} else {
									this.category = Object.assign(this.category, data);
								}

								this.systemMessageService.setSystemMessage(
									SuccessMessage.create(MESSAGE_CATEGORY_SAVE)
								)
							}
							// err => this.systemMessageService.setSystemMessage(
							// 	ErrorMessage.create(err.error)
							// )
						);
				})
		);
	}

	/**
	 * Check are we edit or add category
	 *
	 * @returns {boolean}
	 */
	isEdit() {
		return !!this.category.id
	}

	/**
	 * Get category thumbnail url
	 *
	 * @returns {string}
	 */
	getThumbnailUrl() {
		return this.categoryService.getThumbnailUrl(
			this.category,
			THUMBNAIL_SIZE
		);
	}

	/**
	 * Thumbnail input change event handler
	 *
	 * @param input
	 */
	onThumbnailChange(input: any) {

		if (input.files.length == 0) {
			return;
		}

		let thumbnailFile = input.files[0];

		let formData = new FormData();
		formData.append(REQUEST_PARAM_THUMBNAIL, thumbnailFile);
		
		this.categoryApi.setThumbnail(this.category.id, formData)
			.subscribe(
				(data: Category) => {
					this.category.thumbnail = data.thumbnail;
					this.systemMessageService.setSystemMessage(
						SuccessMessage.create(MESSAGE_THUMBNAIL_SAVE)
					)
				},
				err => this.systemMessageService.setSystemMessage(
					ErrorMessage.create(err.error)
				)
			);
	}

	/**
	 * Unsubscribe all component subscriptions
	 */
	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => {
				subscription.unsubscribe()
			}
		);
	}
}

import { Routes, RouterModule } from "@angular/router";
import { CategoryComponent } from "./category.component";
import { NgModule } from "@angular/core";
import { FormComponent } from "./form/form.component";

const categoryRoutes: Routes = [
	{
		path: '',
		component: CategoryComponent,
		data: { title: 'Категории'},
		children: [
			{
				path: 'add',
				component: FormComponent,
				data: {title: 'Добавить Категорию'}
			},
			{
				path: 'edit/:id',
				component: FormComponent,
				data: {title: 'Редактировать Категорию'}
			}

		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(categoryRoutes)],
	exports: [RouterModule]
})
export class CategoryRoutingModule { }

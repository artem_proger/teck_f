import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import { NotificationsService } from "angular2-notifications";
import { fromEvent } from "rxjs/observable/fromEvent";
import { interval } from "rxjs/observable/interval";
import { User } from "../../../core/models/user";
import { SystemMessageService } from "../../../core/services/system/system-message.service";
import { AuthenticationService } from "../../../core/services/authentication/authentication.service";
import { AppConfig } from "../../../app.config";
import { SystemMessage } from "../../../core/classes/system-message.class";

declare var $: any;

const SELECTOR_FORM = 'form';

@Component({
	selector: 'admin-login',
	templateUrl: './login.component.html',
	styleUrls: [
		'./login.component.css'
	]
})
export class LoginComponent implements OnInit {

	/**
	 * Current user
	 */
	user: User;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];

	/**
	 * Submit form source
	 */
	submitFormSource: any;

	constructor(
		public auth: AuthenticationService,
	    public router: Router,
	    private systemMessageService: SystemMessageService,
		private notificationService: NotificationsService,
	    private el: ElementRef
	) {
		this.user = new User();
	}

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {

		this.submitFormSource = fromEvent($(this.el.nativeElement)
			.find(SELECTOR_FORM)[0], 'submit');

		this.subscriptions$.push(
			/**
			 * Subscribe for system messages
			 */
			this.systemMessageService.getSystemMessageObservable()
				.subscribe(
					(systemMessage: SystemMessage) => {
						if (systemMessage) {
							this.notificationService.success(
								systemMessage.message
							);
						}
					}
				),
			/**
			 * Subscribe on form submit
			 */
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					this.auth.login({
						username: this.user.username,
						password: this.user.password
					}).subscribe(
						data => this.router.navigate([
							AuthenticationService.ROUTE_REDIRECT
						])
					);
				})
		)
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => {
				subscription.unsubscribe();
			}
		);
	}
}

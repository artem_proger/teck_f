import { Component, Input } from "@angular/core";
import { InputItem } from "../../../../core/classes/input-item.class";

@Component({
	selector: 'list-input',
	templateUrl: './list-input.component.html',
	styleUrls: ['./list-input.component.css']
})
export class ListInputComponent {

	/**
	 * Label
	 * @type {string}
	 */
	@Input()
	label: string = '';

	/**
	 * InputItems
	 * @type {Array}
	 */
	@Input()
	items: InputItem[] = [];

	/**
	 * If InputItems are empty array, this message will show
	 */
	@Input()
	noItemsMessage: string;

	/**
	 * Life cycle hooks
	 */
	ngOnInit() {
		if (!this.items) {
			this.items = [];
		}
	}

	/**
	 * Add one more InputItem
	 */
	addValue() {
		this.items.push(new InputItem());
	}

	/**
	 * Remove InputItem
	 * 
	 * @param index
	 */
	removeValue(index: number) {
		this.items.splice(index, 1);
	}
}

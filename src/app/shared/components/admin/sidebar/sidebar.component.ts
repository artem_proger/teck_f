import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { MenuItem } from "../../../../core/classes/menu-item.class";
import { AuthenticationService } from "../../../../core/services/authentication/authentication.service";
import { MENU_ITEMS } from "../../../../core/constants/dashboard.menu";

declare var $ :any;

@Component({
	selector: 'dashboard-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class DashboardSidebarComponent {

	/**
	 * Sidebar MenuItems
	 */
	items: MenuItem[];

	constructor(
		private auth: AuthenticationService,
		public router: Router
	) {
		this.items = MENU_ITEMS;
	}

	/**
	 * Deactivate all MenuItems
	 */
	private deactivateMenuItems() {
		this.items.forEach(item => { item.active = false; });
	}

	/**
	 * Select MenuItem
	 *
	 * @param item
	 */
	selectMenu(item: MenuItem) {
		if (!item.active) {
			this.deactivateMenuItems();
		}
		
		item.active = !item.active;
	}

	/**
	 * Logout
	 */
	logout() {
		this.auth.logout();
		this.router.navigate([AuthenticationService.ROUTE_LOGIN])
	}
}

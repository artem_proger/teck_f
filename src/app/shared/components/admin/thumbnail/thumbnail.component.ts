import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from "@angular/core";

declare var $: any;

const SELECTOR_INPUT_FILE = 'input[type="file"]';

@Component({
	selector: 'thumbnail-upload',
	templateUrl: './thumbnail.component.html',
	styleUrls: ['thumbnail.component.css']
})
export class ThumbnailComponent implements OnInit {

	/**
	 * Text on image
	 */
	@Input()
	text: string;

	/**
	 * Allow multiple download
	 */
	@Input()
	multiple: boolean;

	/**
	 * Background image URL
	 *
	 * @type {string}
	 */
	@Input()
	imagePath: string = '';

	/**
	 * Emit event when open image file dialog
	 *
	 * @type {EventEmitter<any>}
	 */
	@Output() changeInput = new EventEmitter<any>();

	/**
	 * jQuery input object
	 */
	input: any;
	constructor(private el: ElementRef) { }

	/**
	 * Life cycle hooks
	 */
	ngOnInit() {
		this.input = $(this.el.nativeElement.querySelector(SELECTOR_INPUT_FILE))[0];
	}

	/**
	 * Open image file dialog
	 */
	openDialog() {
		this.input.click();
	}

	/**
	 * On change file input
	 */
	onInputChange() {
		this.changeInput.emit(this.input);
	}
}
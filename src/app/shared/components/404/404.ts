import { Component } from "@angular/core";
import '../../../../assets/images/404.jpg';

@Component({
	selector: 'not-found',
	templateUrl: './404.html',
	styleUrls: ['./404.css']
})
export class NotFoundComponent { }

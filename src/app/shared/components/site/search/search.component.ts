import { Component, HostListener, ElementRef } from "@angular/core";
import { Subscription } from "rxjs/Rx";
import { fromEvent } from "rxjs/observable/fromEvent";
import { interval } from "rxjs/observable/interval";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { SearchToggleService } from "../../../../core/services/search-toggle.service";
import { ProductFetcher } from "../../../../core/services/product-fetcher.service";
import { QueryParamService } from "../../../../core/services/system/query-param.service";
import { SystemTitle } from "../../../../core/services/system/system-title.service";
import { PRODUCT_LIST_TITLE } from "../../../../core/constants/products.constant";
import { AppConfig } from "../../../../app.config";
import { ProductService } from "../../../../core/services/product.service";

declare var $: any;

const SELECTOR_FORM = 'form';
const SELECTOR_INPUT = 'input';
const KEY_EXIT = 'Escape';
const EVENT_KEYPRESS = 'document:keydown';

@Component({
	selector: 'site-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.css'],
	providers: [Location]
})
export class SearchComponent {

	/**
	 * jQuery search input object
	 */
	searchInput: any;
	/**
	 * jQuery search form object
	 */
	searchForm: any;

	/**
	 * Search query string
	 *
	 * @type {string}
	 */
	searchQuery: string = '';

	/**
	 * Is current route page products page
	 * @type {boolean}
	 */
	isProductsPage: boolean = false;

	/**
	 * Component subscriptions
	 *
	 * @type {Array}
	 */
	subscriptions$: Subscription[] = [];
	/**
	 * Submit form source
	 */
	submitFormSource: any;
	
	constructor(
		private searchToggleService: SearchToggleService,
		private el: ElementRef,
	    private router: Router,
	    private productFetcher: ProductFetcher,
		private queryParam: QueryParamService,
		private systemTitle: SystemTitle
	) { }

	/**
	 * Life cycle hooks
	 */

	ngOnInit() {

		this.searchForm = $(this.el.nativeElement).find(SELECTOR_FORM);
		this.submitFormSource = fromEvent(this.searchForm[0], 'submit');
		this.searchInput = $(this.el.nativeElement).find(SELECTOR_INPUT);
		this.searchInput.focus();
		this.searchForm.submit(function () {
			return false;
		});

		this.subscriptions$.push(
			/**
			 * Check if current page is 'Products'
			 */
			this.systemTitle.getTitleObservable()
				.subscribe(
					(title: string) => {
						this.isProductsPage = title === PRODUCT_LIST_TITLE;
					}
				),
			/**
			 * If search is active then need to focus on input
			 */
			this.searchToggleService.getSearchStatusObservable()
				.subscribe((isActive: boolean) => {
					if (isActive === true) {
						this.searchInput.focus();
					}
				}),
			/**
			 * On form submit
			 */
			this.submitFormSource
				.throttle((val: any) => interval(AppConfig.THROTTLE_TIME))
				.subscribe(() => {
					this.searchToggleService.setSearchStatus(false);

					var params = {
						title: this.searchQuery
					};

					this.productFetcher.clearFilter();

					if (this.isProductsPage) {
						this.productFetcher.applyParams(params);
						this.queryParam.applyParams(params);
						this.productFetcher.fetch();
					} else {
						this.router.navigate(
							[ProductService.ROUTE_PRODUCTS],
							{
								queryParams: params
							}
						);
					}
				})
		);
	}

	ngOnDestroy() {
		this.subscriptions$.forEach(
			(subscription: Subscription) => {
				subscription.unsubscribe();
			}
		);
	}

	/**
	 * Hide search
	 */
	exitSearch() {
		this.searchToggleService.setSearchStatus(false);
	}

	/**
	 * On press some button
	 *
	 * @param event
	 */
	@HostListener(EVENT_KEYPRESS, ['$event'])
	onPressEscape(event: KeyboardEvent) {
		if (event.code === KEY_EXIT) {
			this.exitSearch();
		}
	}
}
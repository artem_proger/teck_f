import { Component, Input } from "@angular/core";

@Component({
	selector: 'site-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.css']
})
export class FooterComponent {

	/**
	 * Phones array
	 */
	@Input()
	phones: string[];

	/**
	 * City
	 */
	@Input()
	city: string;

	/**
	 * Street
	 */
	@Input()
	street: string;

	/**
	 * ZIP
	 */
	@Input()
	zip: string;

	/**
	 * Schedule array
	 */
	@Input()
	schedule: string[];
}
import { Component, Input } from "@angular/core";
import { Category } from "../../../../core/models/category.model";
import { CategoryService } from "../../../../core/services/category.service";
import { ProductFetcher } from "../../../../core/services/product-fetcher.service";
import { ProductService } from "../../../../core/services/product.service";

@Component({
	selector: 'category-grid',
	templateUrl: './category-grid.component.html',
	styleUrls: ['./category-grid.component.css']
})
export class CategoryGridComponent {

	/**
	 * Categories
	 *
	 * @type {Array}
	 */
	@Input()
	categories: Category[] = [];

	constructor(private categoryService: CategoryService) { }

	/**
	 * Check if Categories is empty
	 * 
	 * @returns {boolean}
	 */
	isCategoriesEmpty() {
		return !this.categories || this.categories.length == 0;
	}

	/**
	 * Get Category thumbnail
	 *
	 * @param category
	 *
	 * @returns {string}
	 */
	getThumbnailUrl(category: Category) {
		return this.categoryService.getThumbnailUrl(
			category,
			CategoryService.SIZE_MEDIUM
		);
	}

	/**
	 * 
	 * @param category
	 * @returns {string}
	 */
	getCategoryLink(category: Category) {
		return (category.children.length > 0)
			? CategoryService.ROUTE_CATEGORY_DETAILS(category.id)
			: ProductService.ROUTE_PRODUCTS;
	}

	/**
	 * Get category route link query params
	 *
	 * @param category
	 */
	getCategoryLinkParams(category: Category) {
		let params = {};

		if (category.children.length === 0) {
			params[ProductFetcher.PARAM_CATEGORY] = category.id;
		}

		return params;
	}
}

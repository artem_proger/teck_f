import { Component, Input } from "@angular/core";
import { SearchToggleService } from "../../../../core/services/search-toggle.service";

@Component({
	selector: 'site-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent {
	/**
	 * Phones array
	 * 
	 * @type {Array}
	 */
	@Input()
	phones: string[] = [];

	/**
	 * Email
	 */
	@Input()
	email: string;

	constructor(private searchToggleService: SearchToggleService) { }

	/**
	 * Show search input
	 */
	activateSearch() {
		this.searchToggleService.setSearchStatus(true);
	}

	/**
	 * Get phone value
	 *
	 * @returns {string}
	 */
	getPhone() {
		return this.phones && this.phones.length > 0 ? this.phones[0] : '';
	}
}
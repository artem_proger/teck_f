import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { NotFoundComponent } from "./components/404/404";
import { DashboardSidebarComponent } from "./components/admin/sidebar/sidebar.component";
import { RouterModule } from "@angular/router";
import { ThumbnailComponent } from "./components/admin/thumbnail/thumbnail.component";
import { ListInputComponent } from "./components/admin/list-input/list-input.component";

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule
	],
	declarations: [
		NotFoundComponent,
		DashboardSidebarComponent,
		ThumbnailComponent,
		ListInputComponent
	],
	exports: [
		DashboardSidebarComponent,
		ThumbnailComponent,
		ListInputComponent
	]
	/*providers: [
		AuthenticationService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: UnauthorizedInterceptor,
			multi: true
		},
		QueryParamService,
		NotificationsService
	]*/
})
export class SharedModule { }

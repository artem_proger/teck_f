import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from "./shared/components/404/404";

const routes: Routes = [
	//{ path: '', redirectTo: '/admin/login', pathMatch: 'full' },
	{ path: '', loadChildren: './pages/site/site.module#SiteModule' },
	{ path: 'admin', loadChildren: './pages/admin/admin.module#AdminModule'},
	{ path: '404', component: NotFoundComponent, data: {
		title: 'Страница не найдена'
	}},
	{ path: '**', redirectTo: '/404' }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule { }


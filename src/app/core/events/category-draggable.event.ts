import { Category } from "../models/category.model";

/**
 * Assigning to other category event data
 */
export class CategoryDraggableEvent {
	/**
	 * Moved category
	 */
	category: Category;

	/**
	 * Categories in one level with moved category (for removing)
	 */
	levelCategories: Category[];
}

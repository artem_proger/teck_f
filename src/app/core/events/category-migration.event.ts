import { Category } from "../models/category.model";

/**
 * Emit attaching one Category to another Category's children or selected some Category
 */
export class CategoryMigrationEvent {

	/**
	 * Child category
	 */
	category: Category;

	/**
	 * Parent category
	 */
	parentCategory?: Category;
}
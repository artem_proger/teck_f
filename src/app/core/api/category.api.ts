import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Category } from "../models/category.model";
import { AppConfig } from "../../app.config";
import { CategoryData } from "../request-data/category.data";
import { CollectionResponseInterface } from "../interfaces/system/collection-response.interface";

@Injectable()
export class CategoryApi {

	/**
	 * Filter categories for parent
	 *
	 * @type {string}
	 */
	static PARAM_PARENT_ID = 'parent-id';

	/**
	 * Check if category has children
	 *
	 * @type {string}
	 */
	static PARAM_HAS_CHILDREN = 'has-children';

	constructor(
		private http: HttpClient
	) { }


	/**
	 * Find all Categories
	 *
	 * @param params
	 *
	 * @returns {Observable<Object>}
	 */
	findAll(params: any = null) {
		return this.http.get<CollectionResponseInterface<Category>>(AppConfig.API_URL + '/category', {
			params: params
		});
	}

	/**
	 * Find Category by id
	 *
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	find(id: string) {
		return this.http.get<Category>(AppConfig.API_URL + '/category/' + id);
	}

	/**
	 * Create Category
	 *
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	create(data: CategoryData) {
		return this.http.post<Category>(AppConfig.API_URL + '/category', data);
	}

	/**
	 * Update Category by id
	 *
	 * @param id
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	update(id: number, data: CategoryData) {
		return this.http.put<Category>(AppConfig.API_URL + '/category/' + id, data);
	}

	/**
	 * Add Category to children of another Category
	 *
	 * @param id
	 * @param parentId
	 *
	 * @returns {Observable<Object>}
	 */
	assign(id: number, parentId: number) {
		return this.http.put<Category>(AppConfig.API_URL + '/category/' + id + '/assign', {
			parent_id: parentId
		});
	}

	/**
	 * Set Category thumbnail
	 *
	 * @param id
	 * @param formData
	 *
	 * @returns {Observable<Object>}
	 */
	setThumbnail(id: number, formData: FormData) {
		return this.http.post(AppConfig.API_URL + '/category/' + id + '/thumbnail', formData);
	}

	/**
	 * Remove Category
	 *
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	delete(id: number) {
		return this.http.delete(AppConfig.API_URL + '/category/' + id);
	}


}
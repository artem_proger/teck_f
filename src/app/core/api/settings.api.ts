import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AppConfig } from "../../app.config";
import { SettingsData } from "../request-data/settings.data";

@Injectable()
export class SettingsApi {

	/**
	 * Get settings which has specific keys
	 *
	 * @type {string}
	 */
	static PARAM_IN = 'in';

	constructor(
		private http: HttpClient
	) { }

	/**
	 * Find all settings
	 *
	 * @param params
	 *
	 * @returns {Observable<Object>}
	 */
	findAll(params: any = null) {
		return this.http.get<any>(AppConfig.API_URL + '/settings', {
			params: params
		});
	}

	/**
	 * Save all settings
	 *
	 * @param data
	 * @returns {Observable<Object>}
	 */
	save(data: SettingsData) {
		return this.http.put<any>(AppConfig.API_URL + '/settings', data);
	}
}

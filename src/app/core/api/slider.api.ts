import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AppConfig } from "../../app.config";
import { SlideData } from "../request-data/slide.data";
import { Slide } from "../models/slide.model";
import { CollectionResponseInterface } from "../interfaces/system/collection-response.interface";

@Injectable()
export class SliderApi {

	constructor(private http: HttpClient) {}

	/**
	 * Find Slide by id
	 *
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	public find(id: number) {
		return this.http.get<Slide>(AppConfig.API_URL + '/slide/' + id);
	}

	/**
	 * Find all Slides
	 *
	 * @returns {Observable<Object>}
	 */
	public findAll() {
		return this.http.get<CollectionResponseInterface<Slide>>(AppConfig.API_URL + '/slide');
	}

	/**
	 * Create Slide
	 *
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	public create(data: SlideData) {
		return this.http.post<Slide>(AppConfig.API_URL + '/slide', data);
	}

	/**
	 * Update Slide by id
	 *
	 * @param id
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	public update(id: number, data: SlideData) {
		return this.http.put<Slide>(AppConfig.API_URL + '/slide/' + id, data);
	}

	/**
	 * Remove Slide
	 *
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	public remove(id: number) {
		return this.http.delete(AppConfig.API_URL + '/slide/' + id);
	}

	/**
	 * Set Slide image
	 *
	 * @param id
	 * @param formData
	 * 
	 * @returns {Observable<Object>}
	 */
	public setImage(id: number, formData: FormData) {
		return this.http.post(AppConfig.API_URL + '/slide/' + id + '/image', formData);
	}
}
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PriceRateData } from "../request-data/price-rate.data";
import { Product } from "../models/product.model";
import { PriceRate } from "../models/price-rate.model";
import { AppConfig } from "../../app.config";
import { CollectionResponseInterface } from "../interfaces/system/collection-response.interface";

/**
 * PriceRate API
 */
@Injectable()
export class PriceRateApi {

	constructor(private http: HttpClient) { }

	/**
	 * @param product
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	create(product: Product, data: PriceRateData) {
		return this.http.post<PriceRate>(AppConfig.API_URL + '/product/' + product.id + '/price-rate', data);
	}

	/**
	 *
	 * @param product
	 * @param id
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	update(product: Product, id: number, data: PriceRateData) {
		return this.http.put<PriceRate>(
			AppConfig.API_URL + '/product/' + product.id + '/price-rate/' + id,
			data
		);
	}

	/**
	 * @param product
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	remove(product: Product, id: number) {
		return this.http.delete(AppConfig.API_URL + '/product/' + product.id + '/price-rate/' + id);
	}

	/**
	 * @param product
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	find(product: Product, id: number) {
		return this.http.get<PriceRate>(AppConfig.API_URL + '/product/' + product.id + '/price-rate/' + id);
	}

	/**
	 * @param productId
	 * @returns {Observable<Object>}
	 */
	findAll(productId: number) {
		return this.http.get<CollectionResponseInterface<PriceRate>>(AppConfig.API_URL + '/product/' + productId + '/price-rate');
	}
}

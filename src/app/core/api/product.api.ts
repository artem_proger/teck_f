import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Product } from "../models/product.model";
import { AppConfig } from "../../app.config";
import { ProductData } from "../request-data/product.data";
import { MoveProductImageData } from "../request-data/move-product-image.data";
import { PaginatedResponseInterface } from "../interfaces/system/paginated-response.interface";

@Injectable()
export class ProductApi {
	
	constructor(private http: HttpClient) { }

	/**
	 * Create Product
	 *
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	create(data: ProductData) {
		return this.http.post<Product>(AppConfig.API_URL + '/product', data)
	}

	/**
	 * Update Product
	 *
	 * @param id
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	update(id: number, data: ProductData) {
		return this.http.put<Product>(AppConfig.API_URL + '/product/' + id, data)
	}

	/**
	 * Remove Product
	 *
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	remove(id: number) {
		return this.http.delete(AppConfig.API_URL + '/product/' + id);
	}

	/**
	 * Find Product by id
	 *
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	find(id: string) {
		return this.http.get<Product>(AppConfig.API_URL + '/product/' + id);
	}

	/**
	 * Find all Products
	 *
	 * @param params
	 *
	 * @returns {Observable<Object>}
	 */
	findAll(params: any = null) {
		return this.http.get<PaginatedResponseInterface<Product>>(AppConfig.API_URL + '/product', {
			params: params
		});
	}

	/**
	 * Upload images to Product gallery
	 *
	 * @param id
	 * @param formData
	 *
	 * @returns {Observable<Object>}
	 */
	uploadGallery(id: number, formData: FormData) {
		return this.http.post(AppConfig.API_URL + '/product/' + id + '/gallery', formData);
	}

	/**
	 * Select main image of Product's gallery
	 *
	 * @param id
	 * @param index
	 *
	 * @returns {Observable<Object>}
	 */
	selectGalleryImage(id: number, index: number) {
		return this.http.put(AppConfig.API_URL + '/product/' + id + '/gallery/select/' + index, null, {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			})
		});
	}

	/**
	 * Remove all images from Product's gallery
	 *
	 * @param id
	 *
	 * @returns {Observable<Object>}
	 */
	removeGallery(id: number) {
		return this.http.delete(AppConfig.API_URL + '/product/' + id + '/gallery');
	}

	/**
	 * Remove image from Product's gallery
	 *
	 * @param id
	 * @param index
	 *
	 * @returns {Observable<Object>}
	 */
	removeGalleryImage(id: number, index: number) {
		return this.http.delete(AppConfig.API_URL + '/product/' + id + '/gallery/remove/' + index);
	}

	/**
	 * Move Product's gallery image from one index place to another
	 *
	 * @param id
	 * @param data
	 *
	 * @returns {Observable<Object>}
	 */
	moveGalleryImage(id: number, data: MoveProductImageData) {
		return this.http.post(AppConfig.API_URL + '/product/' + id + '/gallery/move', data);
	}
}
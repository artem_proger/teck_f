import { Injectable, EventEmitter } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { Product } from "../models/product.model";
import { ProductApi } from "../api/product.api";
import { PaginatedResponseInterface } from "../interfaces/system/paginated-response.interface";

@Injectable()
export class ProductFetcher {

	static PARAM_CATEGORY = 'category-id';
	static PARAM_PRICE_FROM = 'price-from';
	static PARAM_PRICE_TO = 'price-to';

	/**
	 * Current Filter
	 *
	 * @type {{}}
	 */
	filter: any = {};

	/**
	 * Products source
	 * @type {EventEmitter<PaginatedResponseInterface<Product>>}
	 */
	private productsSource: EventEmitter<PaginatedResponseInterface<Product>> = new EventEmitter<PaginatedResponseInterface<Product>>();

	/**
	 * Products observable
	 */
	private productsObservable: Observable<PaginatedResponseInterface<Product>>;

	constructor(private productApi: ProductApi) {
		this.productsObservable = this.productsSource.asObservable();
	}

	/**
	 * Fetch Products by current Filter
	 */
	fetch() {
		this.productApi.findAll(this.filter)
			.subscribe((response: PaginatedResponseInterface<Product>) => {
				this.productsSource.next(response);
			})
	}

	/**
	 * Apply Filter params
	 *
	 * @param params
	 */
	applyParams(params: any) {
		this.filter = Object.assign(this.filter, params);
	}

	/**
	 * Clear Filter
	 */
	clearFilter() {
		this.filter = {}
	}

	/**
	 * Get Products observable
	 *
	 * @returns {Observable<PaginatedResponseInterface<Product>>}
	 */
	getObservable() {
		return this.productsObservable;
	}
}

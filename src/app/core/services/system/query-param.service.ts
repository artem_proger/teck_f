import { Injectable } from "@angular/core";
import { parse, stringify } from 'querystring';
import { Location } from '@angular/common';

const QUERY_PARAMS_IDENTIFIER = '?';

@Injectable()
export class QueryParamService {

	constructor(private location: Location) { }

	getCurrentParams() {
		return (this.location.path().includes(QUERY_PARAMS_IDENTIFIER))
			? parse(this.location.path().split(QUERY_PARAMS_IDENTIFIER)[1])
				: {};
	}

	getCurrentUrl() {
		return this.location.path().split(QUERY_PARAMS_IDENTIFIER)[0];
	}

	applyParams(params: any, extend: boolean = false) {

		if (!extend) {
			this.clearQueryString();
		}

		var newParams = Object.assign(this.getCurrentParams(), params);

		var pathWithParams = this.getCurrentUrl();
		var stringifyParams = stringify(newParams);

		if (stringifyParams) {
			pathWithParams += QUERY_PARAMS_IDENTIFIER + stringifyParams;
		}
		this.location.go(pathWithParams);
	}

	clearQueryString() {
		this.location.go(this.getCurrentUrl());
	}
}

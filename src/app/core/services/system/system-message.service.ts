import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs/Rx";
import { SystemMessage } from "../../classes/system-message.class";
import { AppConfig } from "../../../app.config";
import { NotificationsService } from "angular2-notifications";
import { Detail } from "../../classes/detail.class";

@Injectable()
export class SystemMessageService {
	
	/**
	 * Simple notification plugin options
	 *
	 * @type {{position: string|string[], timeOut: number, lastOnBottom: boolean}}
	 */
	static SIMPLE_NOTIFICATIONS_OPTIONS = {
		position: ["bottom", "left"],
		timeOut: AppConfig.SYSTEM_MESSAGE_DELAY,
		lastOnBottom: true
	};

	/**
	 * SystemMessage source
	 *
	 * @type {BehaviorSubject<SystemMessage>}
	 */
	private systemMessageSource = new BehaviorSubject<SystemMessage>(null);

	/**
	 * SystemMessage observable
	 */
	private systemMessageObservable: Observable<SystemMessage>;

	constructor(private notificationService: NotificationsService) {
		this.systemMessageObservable = this.systemMessageSource.asObservable();
	}

	/**
	 * Set SystemMessage
	 *
	 * @param systemMessage
	 */
	setSystemMessage(systemMessage: SystemMessage) {
		if (systemMessage) {

			var detailsContent = systemMessage.details
				? systemMessage.details.map(
					(detail: Detail) => detail.message
				).join('<br>')
				: '';

			this.notificationService[systemMessage.type](
				systemMessage.message,
				detailsContent
			);
		}
	}

	/**
	 * Get SystemMessage observable
	 *
	 * @returns {Observable<SystemMessage>}
	 */
	getSystemMessageObservable() {
		return this.systemMessageObservable;
	}
}

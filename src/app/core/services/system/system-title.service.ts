import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs/Rx";
import { Title } from "@angular/platform-browser";

@Injectable()
export class SystemTitle {

	/**
	 * Title source
	 *
	 * @type {BehaviorSubject<string>}
	 */
	private titleSource = new BehaviorSubject<string>(' ');

	/**
	 * Title observable
	 */
	private titleObservable: Observable<string>;

	constructor(
		private titleService: Title
	) {
		this.titleObservable = this.titleSource.asObservable();
	}

	/**
	 * Set title
	 *
	 * @param title
	 */
	setTitle(title: string) {
		this.titleService.setTitle(title);
		this.titleSource.next(title);
	}

	/**
	 * Get title observable
	 * 
	 * @returns {Observable<string>}
	 */
	getTitleObservable() {
		return this.titleObservable;
	}
}

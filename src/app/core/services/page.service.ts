import { Injectable } from "@angular/core";
import { SettingsService } from "./settings.service";

const PARAM_CONTACTS = 'contacts';
const PARAM_ABOUT_US = 'about-us';

const TITLE_CONTACTS = 'Контакты';
const TITLE_ABOUT_US = 'О нас';

@Injectable()
export class PageService {
	constructor(private settingsService: SettingsService) { }

	/**
	 * Get Setting key by route param
	 *
	 * @param routeParam
	 *
	 * @returns {any}
	 */
	getSettingKeyByParam(routeParam: string) {
		switch (routeParam) {
			case PARAM_CONTACTS:
				return this.settingsService.getNameMapping().PAGE_CONTACTS;
			case PARAM_ABOUT_US:
				return this.settingsService.getNameMapping().PAGE_ABOUT_US;
			default:
				return false;
		}
	}

	/**
	 * Get title by route param
	 * 
	 * @param routeParam
	 * @returns {any}
	 */
	getTitleByParam(routeParam: string) {
		switch (routeParam) {
			case PARAM_CONTACTS:
				return TITLE_CONTACTS;
			case PARAM_ABOUT_US:
				return TITLE_ABOUT_US;
			default:
				return '';
		}
	}
}

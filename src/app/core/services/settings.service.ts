import { Injectable } from "@angular/core";

const MAPPING_NAME_KEYS = {
	SITE_NAME: 'site_name',
	EMAIL: 'email',
	CITY: 'city',
	STREET: 'street',
	ZIP: 'zip',
	PHONES: 'phones',
	SCHEDULE: 'schedule',
	PAGE_CONTACTS: 'page_contacts',
	PAGE_ABOUT_US: 'page_about_us'
};

const MAPPING_TYPE_KEYS = {
	STRING: 1,
	ARRAY: 2,
	OBJECT: 3,
};

@Injectable()
export class SettingsService {

	/**
	 * Get all settings mapping keys
	 * @returns {{SITE_NAME: string, EMAIL: string, CITY: string, STREET: string, ZIP: string, PHONES: string, SCHEDULE: string, PAGE_CONTACTS: string, PAGE_ABOUT_US: string}}
	 */
	getNameMapping() {
		return MAPPING_NAME_KEYS;
	}

	/**
	 * Get setting type by key
	 *
	 * @param name
	 *
	 * @returns {number}
	 */
	getTypeByName(name: string) {
		switch (name) {
			case MAPPING_NAME_KEYS.SITE_NAME:
			case MAPPING_NAME_KEYS.EMAIL:
			case MAPPING_NAME_KEYS.CITY:
			case MAPPING_NAME_KEYS.STREET:
			case MAPPING_NAME_KEYS.ZIP:
				return MAPPING_TYPE_KEYS.STRING;
			case MAPPING_NAME_KEYS.PHONES:
			case MAPPING_NAME_KEYS.SCHEDULE:
				return MAPPING_TYPE_KEYS.ARRAY;
			default:
				return MAPPING_TYPE_KEYS.STRING;
		}
	}

	/**
	 * Get default value by setting type
	 *
	 * @param type
	 *
	 * @returns {any}
	 */
	getDefaultValueByType(type: number) {
		switch (type) {
			case MAPPING_TYPE_KEYS.STRING:
				return '';
			case MAPPING_TYPE_KEYS.ARRAY:
				return [];
			case MAPPING_TYPE_KEYS.OBJECT:
				return {};
			default:
				return '';
		}
	}

	/**
	 * Get general settings
	 * 
	 * @returns {string[]}
	 */
	getGeneralSettings() {
		return [
			MAPPING_NAME_KEYS.SITE_NAME,
			MAPPING_NAME_KEYS.EMAIL,
			MAPPING_NAME_KEYS.CITY,
			MAPPING_NAME_KEYS.STREET,
			MAPPING_NAME_KEYS.ZIP,
			MAPPING_NAME_KEYS.PHONES,
			MAPPING_NAME_KEYS.SCHEDULE,
		]
	}
}

import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs/Rx";
import { AppConfig } from "../../app.config";
import { Slide } from "../models/slide.model";

const ROUTE_PREFIX = AppConfig.ROUTE_DASHBOARD_PREFIX + '/settings/slider/';

@Injectable()
export class SliderService {

	/**
	 * Image sizes
	 *
	 * @type {string}
	 */
	static SIZE_MINI = 'mini';
	static SIZE_MAX = 'max';

	/**
	 * Routes
	 *
	 * @type {string}
	 */
	static ROUTE_ADD = ROUTE_PREFIX + 'add';
	static ROUTE_EDIT = (id: number) => `${ROUTE_PREFIX}edit/${id}`;
	static ROUTE_HOME = ROUTE_PREFIX;

	/**
	 * Sources
	 *
	 * @type {BehaviorSubject<Slide>}
	 */
	private currentSlideSource = new BehaviorSubject<Slide>(null);
	private addedSlideSource = new BehaviorSubject<Slide>(null);
	private editedSlideSource = new BehaviorSubject<Slide>(null);
	private removedSlideSource = new BehaviorSubject<Slide>(null);

	/**
	 * Observables
	 */
	private currentSlideObservable: Observable<Slide>;
	private addedSlideObservable: Observable<Slide>;
	private editedSlideObservable: Observable<Slide>;
	private removedSlideObservable: Observable<Slide>;

	constructor() {
		this.currentSlideObservable = this.currentSlideSource.asObservable();
		this.addedSlideObservable = this.addedSlideSource.asObservable();
		this.editedSlideObservable = this.editedSlideSource.asObservable();
		this.removedSlideObservable = this.removedSlideSource.asObservable();
	}

	/**
	 * Select Slide
	 *
	 * @param slide
	 */
	selectSlide(slide: Slide) {
		this.currentSlideSource.next(slide);
	}

	/**
	 * Add Slide
	 *
	 * @param slide
	 */
	addSlide(slide: Slide) {
		this.addedSlideSource.next(slide);
	}

	/**
	 * Edit Slide
	 *
	 * @param slide
	 */
	editSlide(slide: Slide) {
		this.editedSlideSource.next(slide);
	}

	/**
	 * Remove Slide
	 *
	 * @param slide
	 */
	removeSlide(slide: Slide) {
		this.removedSlideSource.next(slide);
	}

	/**
	 * Get Slide's image URL
	 *
	 * @param slide
	 * @param size
	 *
	 * @returns {any}
	 */
	public getImageUrl(slide: Slide, size: string) {
		if (!slide.id || !slide.image)
			return null;

		var filename = slide.image.filenames[size];
		return this.getImagePath() + filename;
	}

	/**
	 * Get Slides image path
	 *
	 * @returns {string}
	 */
	public getImagePath() {
		return AppConfig.IMAGES_SLIDE;
	}


	/**
	 * Observable getters
	 * @returns {Observable<Slide>}
	 */
	
	public getCurrentSlideObservable() {
		return this.currentSlideObservable;
	}

	public getAddedSlideObservable() {
		return this.addedSlideObservable;
	}

	public getEditedSlideObservable() {
		return this.editedSlideObservable;
	}

	public getRemovedSlideObservable() {
		return this.removedSlideObservable;
	}

	public getPositionMapping() {
		return {
			LEFT: 0,
			RIGHT: 1
		}
	}
}


import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs/Rx";
import { Product } from "../models/product.model";
import { AppConfig } from "../../app.config";
import { ImagePackage } from "../models/image-package.model";

const ROUTE_PREFIX = `${AppConfig.ROUTE_DASHBOARD_PREFIX}/products/`;

@Injectable()
export class ProductService {

	/**
	 * Image sizes
	 *
	 * @type {string}
	 */
	static SIZE_MINI = 'mini';
	static SIZE_MAX = 'max';

	/**
	 * Routes
	 *
	 * @type {string}
	 */
	static ROUTE_ADD = ROUTE_PREFIX + 'add';
	static ROUTE_EDIT = (id: number) => `${ROUTE_PREFIX}edit/${id}`;
	static ROUTE_PRODUCTS = '/products';
	static ROUTE_PRODUCT_DETAILS = (id: number) => `/products/${id}`;

	/**
	 * Current Product source
	 *
	 * @type {BehaviorSubject<Product>}
	 */
	private currentProductSource = new BehaviorSubject<Product>(new Product());

	/**
	 * Current Product observable
	 */
	private currentProductObservable: Observable<Product>;

	constructor() {
		this.currentProductObservable = this.currentProductSource.asObservable();
	}

	/**
	 * Set Product
	 *
	 * @param product
	 */
	setProduct(product: Product) {
		this.currentProductSource.next(product);
	}

	/**
	 * Get current Product Observable
	 *
	 * @returns {Observable<Product>}
	 */
	getCurrentProductObservable() {
		return this.currentProductObservable;
	}

	/**
	 * Get Products image path
	 *
	 * @returns {string}
	 */
	getImagePath() {
		return AppConfig.IMAGES_PRODUCT;
	}

	/**
	 * Get Product's image URL by model
	 *
	 * @param product
	 * @param size
	 *
	 * @returns {string}
	 */
	getImageUrl(product: Product, size: string) {
		if (!product || !product.id || !product.selected_image) {
			return;
		}

		var filename = product.selected_image[size];

		return this.getImagePath() + filename;
	}

	/**
	 * Get Product's image URL by Product's ImagePackage
	 *
	 * @param imagePackage
	 * @param size
	 *
	 * @returns {string}
	 */
	getImagePackageUrl(imagePackage: ImagePackage, size: string) {
		var filename = imagePackage.filenames[size];
		return this.getImagePath() + filename;
	}
}

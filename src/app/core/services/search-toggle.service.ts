import { Injectable, EventEmitter } from "@angular/core";
import { Observable } from "rxjs/Rx";

@Injectable()
export class SearchToggleService {

	/**
	 * Search status source
	 * @type {EventEmitter<boolean>}
	 */
	private searchStatusSource: EventEmitter<boolean> = new EventEmitter<boolean>();

	/**
	 * Search status observable
	 */
	private searchStatusObservable: Observable<boolean>;

	constructor() {
		this.searchStatusObservable = this.searchStatusSource.asObservable();
	}

	/**
	 * Set search status
	 *
	 * @param isActive
	 */
	setSearchStatus(isActive: boolean) {
		this.searchStatusSource.next(isActive);
	}

	/**
	 * Get search status observable
	 * 
	 * @returns {Observable<boolean>}
	 */
	getSearchStatusObservable() {
		return this.searchStatusObservable;
	}
}

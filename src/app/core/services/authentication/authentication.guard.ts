import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthenticationService } from "./authentication.service";

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private auth: AuthenticationService, private router: Router) { }

	/**
	 * Check can user visit some route
	 *
	 * @param route
	 * @param state
	 *
	 * @returns {boolean}
	 */
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

		// If authenticated successfully
		if (this.auth.isAuthenticated()) {
			return true;
		}

		// Else
		this.router.navigate([AuthenticationService.ROUTE_LOGIN]);

		return false;
	}
}
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AppConfig } from "../../../app.config";
import { AccessTokenResponseInterface } from "../../interfaces/access-token.interface";

const TOKEN_KEY = 'tokenStorage';
const AUTH_HEADER_PREFIX = 'Bearer';

@Injectable()
export class AuthenticationService {

	static ROUTE_LOGIN = 'admin/login';
	static ROUTE_REDIRECT = 'admin/dashboard';

	constructor(
		private http: HttpClient
	) { }

	/**
	 * Handle login
	 *
	 * @param data
	 * @returns {any}
	 */
	login(data: any) {
		return this.http.post<AccessTokenResponseInterface>(AppConfig.API_URL + '/login', data)
			.do(
				(data: any) => this.setUserData(data)
			);
	}

	/**
	 * Set user data in localStorage
	 *
	 * @param response
	 */
	setUserData(response: any) {
		localStorage.setItem(TOKEN_KEY, JSON.stringify(response));
	}

	/**
	 * Logout
	 */
	logout(): void {
		localStorage.removeItem(TOKEN_KEY);
	}

	/**
	 * Fetch user data
	 *
	 * @returns {Observable<Object>}
	 */
	tokenUser() {
		return this.http.get(AppConfig.API_URL + 'token/user');
	}

	/**
	 * Check is user authenticated
	 *
	 * @returns {boolean}
	 */
	isAuthenticated(): boolean {
		return !!localStorage.getItem(TOKEN_KEY);
	}

	/**
	 * Get auth header from localStorage
	 *
	 * @returns {string}
	 */
	getAuthorizationHeader() {
		const parsedTokenStorage = JSON.parse(localStorage.getItem(TOKEN_KEY));
		return (parsedTokenStorage)
			? `${AUTH_HEADER_PREFIX} ${parsedTokenStorage.token}`
			: null;
	}
}

import { Injectable, EventEmitter } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs/Rx";
import { Category } from "../models/category.model";
import { AppConfig } from "../../app.config";
import { CategoryMigrationEvent } from "../events/category-migration.event";

const ROUTE_PREFIX = `${AppConfig.ROUTE_DASHBOARD_PREFIX}/categories/`;

@Injectable()
export class CategoryService {

	/**
	 * Image sizes
	 *
	 * @type {string}
	 */
	static SIZE_THUMBNAIL = '50x50';
	static SIZE_MEDIUM = '300x300';

	/**
	 * Routes
	 *
	 * @type {string}
	 */
	static ROUTE_ADD = ROUTE_PREFIX + 'add';
	static ROUTE_EDIT = (id: number) => `${ROUTE_PREFIX}edit/${id}`;
	static ROUTE_CATEGORY_DETAILS = (id: number) => `/categories/${id}`;

	/**
	 * Sources
	 */
	private categorySource = new BehaviorSubject<CategoryMigrationEvent>({category: new Category()});
	private removedCategorySource = new EventEmitter<Category>();

	/**
	 * Observables
	 */
	private currentCategoryObservable: Observable<CategoryMigrationEvent>;
	private removedCategoryObservable: Observable<Category>;

	constructor() {
		this.currentCategoryObservable = this.categorySource.asObservable();
		this.removedCategoryObservable = this.removedCategorySource.asObservable();
	}

	/**
	 * Set Category and parent Category
	 *
	 * @param category
	 * @param parentCategory
	 */
	setCategory(category: Category, parentCategory: Category = null) {
		this.categorySource.next({
			category: category,
			parentCategory: parentCategory
		});
	}

	/**
	 * Set parent Category and current is empty
	 *
	 * @param parentCategory
	 */
	setParentCategory(parentCategory: Category) {
		this.categorySource.next({
			category: new Category(),
			parentCategory: parentCategory
		});
	}

	/**
	 * Remove Category
	 *
	 * @param category
	 */
	removeCategory(category: Category) {
		this.removedCategorySource.emit(category)
	}

	/**
	 * Get Category image URL
	 *
	 * @param category
	 * @param size
	 *
	 * @returns {any}
	 */
	getThumbnailUrl(category: Category, size: string): string {
		if (!category.id || !category.thumbnail)
			return null;

		var thumbnailFilename = category.thumbnail.filenames[size];

		return AppConfig.IMAGES_CATEGORY + thumbnailFilename;
	}

	/**
	 * Observable getters
	 *
	 * @returns {Observable<CategoryMigrationEvent>}
	 */
	getCurrentCategoryObservable() {
		return this.currentCategoryObservable;
	}
	
	getRemovedCategoryObservable() {
		return this.removedCategoryObservable
	}

}

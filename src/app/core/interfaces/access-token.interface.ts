export interface AccessTokenResponseInterface {
	token: string;
	refresh_token: string;
}

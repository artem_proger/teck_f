export interface CollectionResponseInterface<T> {
	data: T[];
}

import { Meta } from "./meta.interface";

export interface PaginatedResponseInterface<T> {
	meta: Meta;
	data: T[];
}
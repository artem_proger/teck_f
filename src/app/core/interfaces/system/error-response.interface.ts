export interface ErrorResponseInterface {
	status: number;
	code: number;
	message: string;
	errors?: any;
	error?: string;
}

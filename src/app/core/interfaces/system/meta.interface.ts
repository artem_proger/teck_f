export interface Meta {
	page: number;
	total: number;
	per_page: number;
	count: number;
}

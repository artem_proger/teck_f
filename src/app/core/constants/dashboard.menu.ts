import { MenuItem } from "../classes/menu-item.class";

export const MENU_ITEMS: MenuItem[] = [
	{
		title: 'Главная',
		url: '/admin/dashboard',
		icon: 'home'
	},
	{
		title: 'Настройки',
		icon: 'cogs',
		url: 'settings',
		children: [
			{
				title: 'Основные',
				url: 'settings/general'
			},
			{
				title: 'Слайдер',
				url: 'settings/slider'
			},
			{
				title: 'Страница "Контакты"',
				url: 'settings/page/contacts'
			},
			{
				title: 'Страница "О нас"',
				url: 'settings/page/about-us'
			}
		]
	},
	{
		title: 'Категории',
		icon: 'list',
		url: 'categories',
		children: [
			{
				title: 'Добавить',
				url: 'categories/add'
			},
			{
				title: 'Список',
				url: 'categories'
			}
		]
	},
	{
		title: 'Продукты',
		icon: 'shopping-bag',
		url: 'products',
		children: [
			{
				title: 'Добавить',
				url: 'products/add'
			},
			{
				title: 'Список',
				url: 'products/list'
			}
		]
	}
];

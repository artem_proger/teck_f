export interface MoveProductImageData {
	source: number;
	target: number;
}
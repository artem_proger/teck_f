export interface SettingsData {
	site_name?: string;
	email?: string;
	city?: string;
	street?: string;
	zip?: string;
	phones?: string[];
	schedule?: string[];
	page_contacts?: string;
	page_about_us?: string;
}
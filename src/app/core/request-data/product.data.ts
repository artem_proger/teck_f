export interface ProductData {
	title: string;
	description?: string;
	in_store: boolean;
	category_id: number;
}
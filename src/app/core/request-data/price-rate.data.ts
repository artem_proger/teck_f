/**
 * PriceRate request payload data
 */
export interface PriceRateData {

	/**
	 * Range string (Ex. "от 3 кг до 7 кг")
	 */
	range: string;
	
	/**
	 * Price for this rate
	 */
	price: number;

	/**
	 * Price postfix (Ex. "3 грн/[кг]"
	 */
	postfix?: string;
}

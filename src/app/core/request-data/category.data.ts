export interface CategoryData {
	title: string;
	parent_id?: number;
}

export interface SlideData {
	title: string;
	text: string;
	position: number;
}
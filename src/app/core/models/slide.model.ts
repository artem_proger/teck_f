/**
 * Slide model
 */
export class Slide {
	id: number;
	title: string;
	text: string;
	position: number = 0;
	image: any;
}

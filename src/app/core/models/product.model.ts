import { ImagePackage } from "./image-package.model";
import { Category } from "./category.model";

export class Product {
	id: number;
	description: string = '';
	selected_image: any;
	gallery: ImagePackage[];
	category: Category;
	title: string;
	in_store: boolean;
	price_from: number;
	price_to: number;

	constructor() {
		this.in_store = false;
	}
}

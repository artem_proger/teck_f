export class Category {
	id: number;
	title: string;
	thumbnail: any;
	children: Category[];

	constructor() {
		this.children = [];
	}
}

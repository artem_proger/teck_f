import { Selectable } from "../interfaces/selectable.interface";

export class ImagePackage implements Selectable {
	selected: boolean;
	filenames: any;
}

export class PriceRate {
	id: number = null;
	range: string;
	price: number;
	postfix: string;
}

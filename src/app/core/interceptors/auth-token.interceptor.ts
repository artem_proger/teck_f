import { Injectable, Injector } from "@angular/core";
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { AuthenticationService } from "../services/authentication/authentication.service";

const HEADER_AUTHORIZATION = 'Authorization';


/**
 * Attach `Authentication` header token
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	private auth: AuthenticationService;

	constructor(private injector: Injector) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		this.auth = this.injector.get(AuthenticationService);
		const authHeader = this.auth.getAuthorizationHeader();
		
		if (authHeader) {
			return next.handle(
				req.clone({
					headers: req.headers
						.set(HEADER_AUTHORIZATION, authHeader)
				})
			);
		}
		return next.handle(req);
	}
}
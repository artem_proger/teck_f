import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { Router } from "@angular/router";
import { ErrorResponseInterface } from "../interfaces/system/error-response.interface";
import { AuthenticationService } from "../services/authentication/authentication.service";

/**
 * If response is 403, then logout user
 */
@Injectable()
export class UnauthorizedInterceptor implements HttpInterceptor {
	constructor(private router: Router) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(req)
			.do((event: HttpEvent<any>) => {
				// some
			}, (err: ErrorResponseInterface) => {
				if (err.status == 401) {
					this.router.navigate([AuthenticationService.ROUTE_LOGIN])
				}
			});

	}
}

import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { SystemMessageService } from "../services/system/system-message.service";
import { ErrorResponseInterface } from "../interfaces/system/error-response.interface";
import { ErrorMessage, TYPE } from "../classes/error-message.class";

@Injectable()
export class ErrorMessageInterceptor implements HttpInterceptor {

	constructor(private systemMessageService: SystemMessageService) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(req)
			.do(
				(event: HttpEvent<any>) => {

				},
				(err: HttpErrorResponse) => {
					if (err.error.status === TYPE) {
						this.systemMessageService.setSystemMessage(
							ErrorMessage.create(err.error)
						);
					}
				}
			);
	}
}
import { SystemMessage } from "./system-message.class";
import { Detail } from "./detail.class";

const TYPE = 'success';

export class SuccessMessage implements SystemMessage {

	/**
	 * Message text
	 */
	message: string;

	/**
	 * Message type
	 *
	 * @type {string}
	 */
	type: string = TYPE;

	/**
	 * Message details
	 */
	details: Detail[];

	/**
	 * Create success message
	 * @param message
	 * @returns {SuccessMessage}
	 */
	static create(message: string) {
		var systemMessage = new SuccessMessage();
		systemMessage.message = message;

		return systemMessage;
	}

}
/**
 * One of SystemMessage's details text
 */
export class Detail {
	message: string;
}
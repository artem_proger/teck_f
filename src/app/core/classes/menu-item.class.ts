/**
 * Slider menu item
 */
export class MenuItem {
	title: string;
	url: string;
	icon?: string;
	home?: boolean;
	active?: boolean;
	children?: MenuItem[];

	constructor() {
		this.active = false;
	}
}

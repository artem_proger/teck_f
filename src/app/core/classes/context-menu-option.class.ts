/**
 * Context menu action class
 */
export class ContextMenuOption {
	title: string;
	handler: any;

	constructor(title: string, handler: any) {
		this.title = title;
		this.handler = handler
	}

}

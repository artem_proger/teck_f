import { Detail } from "./detail.class";

export class SystemMessage {
	
	/**
	 * Message text
	 */
	message: string;

	/**
	 * Message type
	 */
	type: string;

	/**
	 * Message details
	 */
	details: Detail[];
}
/**
 * Input item for list input (array value)
 */
export class InputItem {

	constructor(value: any = null) {
		this.value = value;
	}

	value: any;
}

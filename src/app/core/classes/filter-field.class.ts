/**
 * Filter field model
 */
export class FilterField<T> {

	/**
	 * Field value
	 */
	value: T;

	/**
	 * Selected indicator
	 *
	 * @type {boolean}
	 */
	selected: boolean = false;

	constructor(value: T) {
		this.value = value;
	}
}
import { Injectable } from "@angular/core";
import { Category } from "../models/category.model";

/**
 * Store for category tree
 */
@Injectable()
export class CategoryTree {

	/**
	 * Category tree
	 *
	 * @type {Array}
	 */
	categories: Category[] = [];

	/**
	 * Set tree categories
	 *
	 * @param categories
	 */
	setCategories(categories: Category[]) {
		this.categories = categories;
	}

	/**
	 * Get tree categories
	 *
	 * @returns {Category[]}
	 */
	getCategories(): Category[] {
		return this.categories;
	}

	/**
	 * Add category to the tree
	 *
	 * @param category
	 * @param parentCategory
	 */
	addCategory(category: Category, parentCategory: Category = null) {
		if (parentCategory) {
			parentCategory.children.push(category);
		} else {
			this.categories.push(category);
		}
	}

	/**
	 * Remove category from the tree
	 *
	 * @param category
	 * @param levelCategories
	 */
	removeCategory(category: Category, levelCategories: Category[]) {
		var index = levelCategories.indexOf(category);
		levelCategories.splice(index, 1);
	}

	/**
	 * Find category in the tree
	 *
	 * @param id
	 * @returns {Category}
	 */
	findCategory(id: number) {
		return this.findCategoryInList(id, this.categories);
	}

	/**
	 * Check if current category is child of checked category
	 *
	 * @param currentCategory
	 * @param checkedCategory
	 *
	 * @returns {boolean}
	 */
	isChild(currentCategory: Category, checkedCategory: Category): boolean {
		if (currentCategory.children.length == 0) {
			return false;
		}

		return currentCategory.children.some(category => {
			return (category == checkedCategory || this.isChild(category, checkedCategory));
		});
	}

	/**
	 * Find category in tree categories collection
	 *
	 * @param id
	 * @param categories
	 * @returns {Category}
	 */
	private findCategoryInList(id: number, categories: Category[]) {
		let searchedCategory: Category = null;
		categories.forEach((category: Category) => {
			if (category.id == id) {
				searchedCategory = category;
				return;
			}

			if (category.children && category.children.length > 0) {
				searchedCategory = this.findCategoryInList(id, category.children);
				return;
			}
		});

		return searchedCategory;
	}
}

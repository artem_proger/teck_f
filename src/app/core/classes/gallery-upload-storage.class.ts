import { Injectable } from "@angular/core";
import { ImagePackage } from "../models/image-package.model";

/**
 * Gallery upload ImagePackages storage
 */
@Injectable()
export class GalleryUploadStorage {

	/**
	 * ImagePackages
	 */
	private imagePackages: ImagePackage[];

	/**
	 * @param imagePackages
	 */
	setImagePackages(imagePackages: ImagePackage[]) {
		this.imagePackages = imagePackages;
	}

	/**
	 * @returns {ImagePackage[]}
	 */
	getImagePackages(): ImagePackage[] {
		return this.imagePackages;
	}

	/**
	 * @param imagePackage
	 */
	selectImagePackage(imagePackage: ImagePackage) {
		this.imagePackages.forEach((imagePackage: ImagePackage) => {
			imagePackage.selected = false;
		});

		imagePackage.selected = true;
	}

	/**
	 * Get index for ImagePackage
	 *
	 * @param imagePackage
	 *
	 * @returns {number}
	 */
	getIndex(imagePackage: ImagePackage) {
		return this.imagePackages.indexOf(imagePackage);
	}

	/**
	 * Move one ImagePackage in position to another ImagePackage
	 *
	 * @param sourcePackage
	 * @param targetPackage
	 *
	 * @returns {ImagePackage[]}
	 */
	moveImagePackage(sourcePackage: ImagePackage, targetPackage: ImagePackage) {
		let sourceIndex = this.getIndex(sourcePackage);
		let targetIndex = this.getIndex(targetPackage);

		if (targetIndex >= this.imagePackages.length) {
			var k = targetIndex - this.imagePackages.length + 1;
			while (k--) {
				this.imagePackages.push(undefined);
			}
		}
		this.imagePackages.splice(targetIndex, 0, this.imagePackages.splice(sourceIndex, 1)[0]);
		
		return this.imagePackages; // for testing
	}

	/**
	 * @param imagePackage
	 */
	removeImagePackage(imagePackage: ImagePackage) {
		let index = this.getIndex(imagePackage);
		let isSelected = imagePackage.selected;

		this.imagePackages.splice(index, 1);

		if (isSelected && this.imagePackages.length > 0) {
			this.imagePackages[0].selected = true;
		}
	}
}
import { SystemMessage } from "./system-message.class";
import { Detail } from "./detail.class";
import { ErrorResponseInterface } from "../interfaces/system/error-response.interface";

export const TYPE = 'error';

export class ErrorMessage implements SystemMessage {

	/**
	 * Message text
	 */
	message: string;

	/**
	 * Message type
	 * 
	 * @type {string}
	 */
	type: string = TYPE;

	/**
	 * Message details
	 *
	 * @type {Array}
	 */
	details: Detail[] = [];

	/**
	 * Create ErrorMessage
	 * 
	 * @param errorResponse
	 * 
	 * @returns {ErrorMessage}
	 */
	static create(errorResponse: ErrorResponseInterface) {
		var systemMessage = new ErrorMessage();
		systemMessage.message = errorResponse.message;

		if (errorResponse.errors) {
			for (var key in errorResponse.errors) {
				if (errorResponse.errors.hasOwnProperty(key)) {
					errorResponse.errors[key].forEach((message: string) => {
						systemMessage.details.push({
							message: message
						})
					});
				}
			}
		}

		return systemMessage;
	}
}
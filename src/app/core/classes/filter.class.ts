import { FilterField } from "./filter-field.class";
import { Range } from "./../classes/range.class";

const PARAM_TITLE = 'title';
const PARAM_PRICE_FROM = 'price-from';
const PARAM_PRICE_TO = 'price-to';
const PARAM_IN_STORE = 'in-store';
const PARAM_CATEGORY_ID = 'category-id';
const PARAMS_DELIMITER = '-';
const PROPERTY_DELIMITER = '_';
const PROPERTY_VALUE_KEY = 'value';
const PROPERTY_SELECTED_KEY = 'selected';

/**
 * Object for filtering Products
 */
export class ProductFilter {

	/**
	 * Title filter
	 */
	title: FilterField<string>;

	/**
	 * Category filter
	 */
	category_id: FilterField<number>;

	/**
	 * Price filter
	 */
	price: FilterField<Range>;

	/**
	 * In store filter
	 */
	in_store: FilterField<number>;

	constructor(priceRange: Range) {
		this.title = new FilterField('');
		this.category_id = new FilterField(null);
		this.price = new FilterField(priceRange);
		this.in_store = new FilterField(0);
	}

	/**
	 * Creation from query params & default price Range
	 *
	 * @param params
	 * @param defaultPriceRange
	 * @returns {ProductFilter}
	 */
	static create(params: any, defaultPriceRange: Range) {

		var filter = new ProductFilter(defaultPriceRange);

		for (var paramName in params) {

			var paramValue = params[paramName];
			switch (paramName) {
				case PARAM_TITLE:
				case PARAM_CATEGORY_ID:
				case PARAM_IN_STORE:
					let propertyName = paramName.replace(
						PARAMS_DELIMITER, PROPERTY_DELIMITER
					);
					filter[propertyName].selected = true;
					filter[propertyName].value = paramValue;
					break;
				case PARAM_PRICE_FROM:
					filter.price.selected = true;
					filter.price.value.from = paramValue;
					break;
				case PARAM_PRICE_TO:
					filter.price.selected = true;
					filter.price.value.to = paramValue;
					break;
				default:
					break;
			}
		}

		return filter;
	}

	/**
	 * Get query params object
	 *
	 * @returns {any}
	 */
	getParamsObj() {
		var params = [{}];
		
		for (let property in this) {
			if (this[property][PROPERTY_SELECTED_KEY]) {
				params.push(this.getParamsByProperty(property));
			}
		}

		return Object.assign.apply(Object, params);
	}

	/**
	 * Reset filter
	 */
	reset() {
		for (let property in this) {
			this[property][PROPERTY_SELECTED_KEY] = false;
		}
	}

	/**
	 * Get query params by property name
	 *
	 * @param property
	 *
	 * @returns {{}}
	 */
	private getParamsByProperty(property: string) {
		var params = {};
		var key;
		var value;
		if (typeof this[property][PROPERTY_VALUE_KEY] == 'object') {
			for (let valueKey in this[property][PROPERTY_VALUE_KEY]) {
				key = property.replace(PROPERTY_DELIMITER, PARAMS_DELIMITER)
					+ PARAMS_DELIMITER
					+ valueKey;
				value = this[property][PROPERTY_VALUE_KEY][valueKey];
				params[key] = value;
			}
		} else {
			key = property.replace(PROPERTY_DELIMITER, PARAMS_DELIMITER);
			value = this[property][PROPERTY_VALUE_KEY];
			params[key] = value;
		}

		return params;
	}
}

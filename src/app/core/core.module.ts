import { NgModule } from "@angular/core";
import { SearchToggleService } from "./services/search-toggle.service";
import { CategoryApi } from "./api/category.api";
import { SliderApi } from "./api/slider.api";
import { AuthenticationService } from "./services/authentication/authentication.service";
import { PageService } from "./services/page.service";
import { ProductFetcher } from "./services/product-fetcher.service";
import { PriceRateApi } from "./api/price-rate.api";
import { SettingsApi } from "./api/settings.api";
import { QueryParamService } from "./services/system/query-param.service";
import { ProductService } from "./services/product.service";
import { SettingsService } from "./services/settings.service";
import { SliderService } from "./services/slider.service";
import { ProductApi } from "./api/product.api";
import { SystemMessageService } from "./services/system/system-message.service";
import { SystemTitle } from "./services/system/system-title.service";
import { CategoryService } from "./services/category.service";
import { AuthGuard } from "./services/authentication/authentication.guard";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorMessageInterceptor } from "./interceptors/error-message.interceptor";
import { AuthInterceptor } from "./interceptors/auth-token.interceptor";
import { UnauthorizedInterceptor } from "./interceptors/unauthorized.interceptor";

const PROVIDERS = [
	CategoryApi,
	PriceRateApi,
	ProductApi,
	SettingsApi,
	SliderApi,
	AuthenticationService,
	AuthGuard,
	QueryParamService,
	SystemMessageService,
	SystemTitle,
	CategoryService,
	PageService,
	ProductService,
	ProductFetcher,
	SearchToggleService,
	SettingsService,
	SliderService
];

const INTERCEPTORS = [
	{
		provide: HTTP_INTERCEPTORS,
		useClass: AuthInterceptor,
		multi: true
	},
	{
		provide: HTTP_INTERCEPTORS,
		useClass: ErrorMessageInterceptor,
		multi: true
	},
	{
		provide: HTTP_INTERCEPTORS,
		useClass: UnauthorizedInterceptor,
		multi: true
	}

];

@NgModule({
	imports: [
		HttpClientModule
	],
	providers: [
		...PROVIDERS,
		...INTERCEPTORS
	]
})
export class CoreModule { }
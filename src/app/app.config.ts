export class AppConfig {
	static BACKEND_URL = 'http://127.0.0.1:8000';
	static API_URL = `${AppConfig.BACKEND_URL}/api`;
	static API_RESOURCES = `${AppConfig.BACKEND_URL}/resources`;
	static IMAGES_CATEGORY = `${AppConfig.API_RESOURCES}/images/category/`;
	static IMAGES_PRODUCT = `${AppConfig.API_RESOURCES}/images/product/`;
	static IMAGES_SLIDE = `${AppConfig.API_RESOURCES}/images/slide/`;
	static SYSTEM_MESSAGE_DELAY = 5000;
	static THROTTLE_TIME = 5000;
	static ROUTE_404 = '404';
	static ROUTE_ADMIN_PREFIX = '/admin';
	static ROUTE_DASHBOARD_PREFIX = `${AppConfig.ROUTE_ADMIN_PREFIX}/dashboard`;

}

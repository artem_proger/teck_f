import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from "./app-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from "./shared/shared.module";
import { NgDragDropModule } from "ng-drag-drop/index";
import { LoadingBarRouterModule } from "@ngx-loading-bar/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CoreModule } from "./core/core.module";
import { SimpleNotificationsModule } from "angular2-notifications";
import { ErrorMessageInterceptor } from "./core/interceptors/error-message.interceptor";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { AuthInterceptor } from "./core/interceptors/auth-token.interceptor";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        CoreModule,
        AppRoutingModule,
        SharedModule,
        LoadingBarRouterModule,
        NgbModule.forRoot(),
        NgDragDropModule.forRoot(),
        SimpleNotificationsModule.forRoot(),
        BrowserAnimationsModule,
        //HttpClientModule
    ],
    declarations: [
        AppComponent
    ],
    /*providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorMessageInterceptor,
            multi: true
        }
    ],*/
    bootstrap: [ AppComponent ]
})
export class AppModule { }
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Subscription } from "rxjs/Rx";
import { SystemTitle } from "./core/services/system/system-title.service";
import { SystemMessageService } from "./core/services/system/system-message.service";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

    /**
     * Simple notifications options
     *
     * @type {{position: string|string[], timeOut: number, lastOnBottom: boolean}}
     */
    notificationOptions: any = SystemMessageService.SIMPLE_NOTIFICATIONS_OPTIONS;

	/**
     * Component subscriptions
     *
     * @type {Array}
     */
    subscriptions$: Subscription[] = [];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private systemTitle: SystemTitle
    ) { }

	/**
	 * Life cycle hooks
     */
    
    ngOnInit() {

        this.subscriptions$.push(
	        /**
             * Set title by route data
             */
            this.router.events
                .filter(event => event instanceof NavigationEnd)
                .map(() => this.activatedRoute)
                .map((route) => {
                    while (route.firstChild) route = route.firstChild;
                    return route;
                })
                .filter((route) => route.outlet === 'primary')
                .mergeMap((route) => route.data)
                .subscribe((event) => {
                    if (event['title']) {
                        this.systemTitle.setTitle(event['title']);
                    }
                }),
	        /**
             * When go to another page then scroll to top
             */
            this.router.events
                .subscribe((event) => {
                    if (!(event instanceof NavigationEnd)) {
                        return;
                    }
                    window.scrollTo(0, 0);
                })
        );
    }

    ngOnDestroy() {
        this.subscriptions$.forEach(
            (subscription: Subscription) => {
                subscription.unsubscribe();
            }
        );
    }

}
// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

// RxJS
import 'rxjs';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'jquery-zoom/jquery.zoom.js';
import 'font-awesome/css/font-awesome.min.css'
import 'ion-rangeslider/css/ion.rangeSlider.css'
import 'ion-rangeslider/css/ion.rangeSlider.skinModern.css'
import 'ion-rangeslider/js/ion.rangeSlider.js'
import './assets/css/styles.css';
import './assets/css/site/price-range.css';
import './assets/css/site/animate.css';
import './assets/css/site/main.css';
import './assets/css/site/responsive.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'aos/dist/aos.css';
import 'aos/dist/aos.js';
import 'jquery';
import 'ion-rangeslider/img/sprite-skin-flat.png';
// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...